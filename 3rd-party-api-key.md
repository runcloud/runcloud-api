<div class="row">
<div class="col col-xl-6">

#### Create new API Key

###### Parameters

<div class="table-respsonsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>label</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Name of the API Key</td>
        </tr>
        <tr>
            <td>
                <span>service</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"cloudflare", "linode" or "digitalocean"</td>
        </tr>
        <tr>
            <td>
                <span>username</span>
                <span>required is service is "cloudflare"</span>
            </td>
            <td>String</td>
            <td>The username or email to authenticate with the secret key</td>
        </tr>
        <tr>
            <td>
                <span>secret</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>The secret key</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /settings/externalapi`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/settings/externalapi
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/settings/externalapi \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "label": "Testing",
    "service": "digitalocean",
    "secret": "k2jh35jk2h2k6jh2981"
}'
```

###### Response

```json
{
	"id": 28,
	"label": "Testing",
	"username": null,
	"secret": "k2jh35jk2h2k6jh2981",
	"service": "Digital Ocean",
	"created_at": "2019-06-30 20:56:22"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List all 3rd Party API Key

###### Query String Parameters

<table class="table">
    <tbody>
        <tr>
            <td>
                <span>search</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Searching string if you wanted to search from list of api keys</td>
        </tr>
    </tbody>
</table>

</div>
<div class="col col-xl-6 dark">

`GET /settings/externalapi`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/settings/externalapi
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/settings/externalapi \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 2,
			"label": "Cloudflare",
			"username": "test@test.com",
			"secret": "23jk22k98as928923x992x892...",
			"service": "Cloudflare",
			"created_at": "2019-04-08 14:21:47"
		},
		{
			"id": 1,
			"label": "DO8",
			"username": null,
			"secret": "ksjdfh2k352ls098k...",
			"service": "Digital Ocean",
			"created_at": "2019-04-06 00:43:43"
		},
		{
			"id": 28,
			"label": "Testing",
			"username": null,
			"secret": "k2jh35jk2h2k6jh2981",
			"service": "Digital Ocean",
			"created_at": "2019-06-30 20:56:22"
		}
	]
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### 3rd Party API object

</div>
<div class="col col-xl-6 dark">

`GET /settings/externalapi/{apiId}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/settings/externalapi/{apiId}
```

###### Request

```bash
curl --request GET \
    --url https://{{ API_BASE_PATH }}/settings/externalapi/28 \
    -u YOUR_API_KEY:YOUR_API_SECRET \
    --header 'accept: application/json' \
    --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 28,
	"label": "Testing",
	"username": null,
	"secret": "k2jh35jk2h2k6jh2981",
	"service": "Digital Ocean",
	"created_at": "2019-06-30 20:56:22"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Update the API Key

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>label</span>
                <span class="text-danger">required</span>
            </td>
            <td>Name of the API Key</td>
        </tr>
        <tr>
            <td>
                <span>username</span>
                <span class="text-danger">required</span>
            </td>
            <td>The username or email to authenticate with the secret key</td>
        </tr>
        <tr>
            <td>
                <span>secret</span>
                <span class="text-danger">required</span>
            </td>
            <td>The secret key</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /settings/externalapi/{apiId}`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/settings/externalapi/{apiId}
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/settings/externalapi/28 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "label": "DO Key Edited",
    "secret": "k2jh35jk2h2k6jh2981"
}'
```

###### Response

```json
{
	"id": 28,
	"label": "DO Key Edited",
	"username": "testing@test.com",
	"secret": "k2jh35jk2h2k6jh2981",
	"service": "Digital Ocean",
	"created_at": "2019-06-30 20:56:22"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete the API Key

</div>
<div class="col col-xl-6 dark">

`DELETE /settings/externalapi/{apiId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/settings/externalapi/{apiId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/settings/externalapi/28 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 28,
	"label": "DO Key Edited",
	"username": null,
	"secret": "k2jh35jk2h2k6jh2981",
	"service": "Digital Ocean",
	"created_at": "2019-06-30 20:56:22"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Create cron job

###### Parameters

<div clss="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>label</span>
                    <span class="text-danger">required</span>
                </td>
                <td>Label of the Cron Job</td>
            </tr>
            <tr>
                <td>
                    <span>username</span>
                    <span class="text-danger">required</span>
                </td>
                <td>Linux System User that will be running this job</td>
            </tr>
            <tr>
                <td>
                    <span>command</span>
                    <span class="text-danger">required</span>
                </td>
                <td>Full command of job to be running/td>
            </tr>
            <tr>
                <td>
                    <span>minute</span>
                    <span class="text-danger">required</span>
                </td>
                <td>Crontab syntax for minute</td>
            </tr>
            <tr>
                <td>
                    <span>hour</span>
                    <span class="text-danger">required</span>
                </td>
                <td>Crontab syntax for hour</td>
            </tr>
            <tr>
                <td>
                    <span>dayOfMonth</span>
                    <span class="text-danger">required</span>
                </td>
                <td>Crontab syntax for the day of month</td>
            </tr>
            <tr>
                <td>
                    <span>month</span>
                    <span class="text-danger">required</span>
                </td>
                <td>Crontab syntax for month</td>
            </tr>
            <tr>
                <td>
                    <span>dayOfWeek</span>
                    <span class="text-danger">required</span>
                </td>
                <td>Crontab syntax for the day of week</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/cronjobs`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/cronjobs
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/cronjobs \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "label": "my first job",
    "username": "root",
    "command": "/bin/bash /root/clearitem.sh",
    "minute": "*",
    "hour": "*/2",
    "dayOfMonth": "*",
    "month": "*",
    "dayOfWeek": "*"
}'
```

###### Response

```json
{
	"id": 9,
	"label": "my first job",
	"username": "root",
	"time": "* */2 * * *",
	"command": "/bin/bash /root/clearitem.sh",
	"created_at": "2019-06-23 13:58:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List jobs

###### Query String Parameters

<table class="table">
    <tbody>
        <tr>
            <td>
                <span>search</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Searching string if you wanted to search from list of jobs</td>
        </tr>
    </tbody>
</table>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/cronjobs`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/cronjobs
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/cronjobs \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 9,
			"label": "my first job",
			"username": "root",
			"time": "* */2 * * *",
			"command": "/bin/bash /root/clearitem.sh",
			"created_at": "2019-06-23 13:58:29"
		},
		{
			"id": 7,
			"label": "test",
			"username": "runcloud",
			"time": "* * * * *",
			"command": "/bin/bash kjhj",
			"created_at": "2019-05-14 18:43:33"
		}
	],
	"meta": {
		"pagination": {
			"total": 2,
			"count": 2,
			"per_page": 15,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Job object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/cronjobs/{jobId}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/cronjobs/{jobId}
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/cronjobs/9 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 9,
	"label": "my first job",
	"username": "root",
	"time": "* */2 * * *",
	"command": "/bin/bash /root/clearitem.sh",
	"created_at": "2019-06-23 13:58:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Rebuild jobs

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/cronjobs/rebuild`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/cronjobs/rebuild
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/cronjobs/rebuild \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json

```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete job

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/cronjobs/{jobId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/cronjobs/{jobId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/cronjobs/9 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 9,
	"label": "my first job",
	"username": "root",
	"time": "* */2 * * *",
	"command": "/bin/bash /root/clearitem.sh",
	"created_at": "2019-06-23 13:58:29"
}
```

</div>
</div>

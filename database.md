<div class="row">
<div class="col col-xl-6">

#### Create database

###### Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>name</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Name of the Database</td>
            </tr>
            <tr>
                <td>
                    <span>collation</span>
                    <span>optional</span>
                </td>
                <td>String</td>
                <td>Database collation. You can get this value from Static Data API.</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/databases`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/databases
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/databases \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "name": "my_database"
}'
```

###### Response

```json
{
	"id": 87,
	"name": "my_database",
	"collation": null,
	"created_at": "2019-06-21 05:04:38"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List databases

###### Query String Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>search</span>
                    <span>optional</span>
                </td>
                <td>String</td>
                <td>Searching string if you wanted to search from list of database</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/databases`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/databases
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/databases \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 81,
			"name": "appDibbert_1559499789",
			"collation": null,
			"created_at": "2019-06-02 18:23:09"
		},
		{
			"id": 87,
			"name": "my_database",
			"collation": null,
			"created_at": "2019-06-21 05:04:38"
		}
	],
	"meta": {
		"pagination": {
			"total": 2,
			"count": 2,
			"per_page": 15,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Database object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/databases/{databaseId}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/databases/{databaseId}
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/databases/87 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 87,
	"name": "my_database",
	"collation": null,
	"created_at": "2019-06-21 05:04:38"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete database

###### Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>deleteUser</span>
                    <span>optional</span>
                </td>
                <td>Boolean</td>
                <td>Delete all database user associated with this database</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/databases/{databaseId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/databases/{databaseId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/databases/88 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "deleteUser": true
}'
```

###### Response

```json
{
	"id": 88,
	"name": "my_database2",
	"collation": null,
	"created_at": "2019-06-21 07:01:42"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Create database user

###### Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>username</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Username of the database user</td>
            </tr>
            <tr>
                <td>
                    <span>password</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Password for the database user</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/databaseusers`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/databaseusers
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/databaseusers \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "username": "test_user",
    "password": "mysupersecretpassword"
}'
```

###### Response

```json
{
	"id": 59,
	"username": "test_user",
	"created_at": "2019-06-21 07:49:43"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List database users

###### Query String Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>search</span>
                    <span>optional</span>
                </td>
                <td>String</td>
                <td>Searching string if you wanted to search from list of database user</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/databaseusers`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/databaseusers
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/databaseusers \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 59,
			"username": "test_user",
			"created_at": "2019-06-21 07:49:43"
		},
		{
			"id": 5,
			"username": "test2",
			"created_at": "2019-04-15 02:59:48"
		}
	],
	"meta": {
		"pagination": {
			"total": 2,
			"count": 2,
			"per_page": 15,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Database User object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/databaseusers/{databaseUserId}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/databaseusers/{databaseUserId}
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/databaseusers/59 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 59,
	"username": "test_user",
	"created_at": "2019-06-21 07:49:43"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Update Database User password

###### Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>password</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Password for the database user</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/databaseusers/{databaseUserId}`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/databaseusers/{databaseUserId}
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/databaseusers/59 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "password": "my0th3rs3cr37p4ss"
}'
```

###### Response

```json
{
	"id": 59,
	"username": "test_user",
	"created_at": "2019-06-21 07:49:43"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete Database User

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/databaseusers/{databaseUserId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/databaseusers/{databaseUserId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/databaseusers/5 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 5,
	"username": "test2",
	"created_at": "2019-04-15 02:59:48"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Attach Database User to Database

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>id</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>The id of Database User to attach to the database</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/databases/{databaseId}/grant`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/databases/{databaseId}/grant
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/databases/87/grant \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "id": 59
}'
```

###### Response

```json
{
	"id": 59,
	"username": "test_user",
	"created_at": "2019-06-21 07:49:43"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List granted users of a Database

###### Query String Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>search</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Searching string if you wanted to search from list of granted Database User</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/databases/{databaseId}/grant`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/databases/{databaseId}/grant
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/databases/87/grant \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 59,
			"username": "test_user",
			"created_at": "2019-06-21 07:49:43"
		}
	],
	"meta": {
		"pagination": {
			"total": 1,
			"count": 1,
			"per_page": 15,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Revoke granted users from a Database

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>id</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Id of a Database User to revoke from the Database</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/databases/{databaseId}/grant`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/databases/{databaseId}/grant
```

###### Request

```bash
 --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/databases/87/grant \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "id": 59
}'
```

###### Response

```json
{
	"id": 59,
	"username": "test_user",
	"created_at": "2019-06-21 07:49:43"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Latest health data

</div>
<div class="col col-xl-6 dark">

`GET /servers/{id}/health/latest`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{id}/health/latest
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/health/latest \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"totalMemory": 974.87891,
	"availableMemory": 364.71094,
	"usedMemory": 610.17,
	"totalDiskSpace": 9376.75162,
	"availableDiskSpace": 5574.00064,
	"usedDiskSpace": 3802.75,
	"loadAverage": 0,
	"updated_at": "2019-06-21 02:47:20"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Clean disk

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{id}/health/diskcleaner`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{id}/health/diskcleaner
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/health/diskcleaner \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json

```

</div>
</div>

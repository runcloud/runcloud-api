This API docs will evolve from time to time depending on new features being added. The API Version you are currently viewing is **Version 2**. Please treat our API as beta stage and may evolve from time to time without further notice.

#### Get Started

To get started with API, you need a RunCloud account. If you don't have one, you can create [a new account here](https://manage.runcloud.io/auth/register).

Once you have created the account, you can use the API Key and API Secret that you can get from the profile menu to use our API.

##### API endpoint

The base URL for our API is:

```
https://{{ API_BASE_PATH }}/
```

<div class="alert alert-warning">All requests to the API must be made over HTTPS.</div>

<div class="alert alert-warning">All request and response will be in JSON formatted output.</div>

#### Headers

You need to make sure all the requests to our API have `Content-Type: application/json` and `Accept: application/json` headers.

### Pagination

If the API Endpoint returns array of data, you may adjust the number of result by adding `perPage` to query string. You also can move to the next page by using `page` inside the query string. The maximum `perPage` query is 40.

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/ping \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

#### Authentication

<div class="alert alert-success">We are using Basic Authentication for Authentication</div>

For every request made to RunCloud API, you need to supply API Key and API Secret as **Basic Authentication**.

You can get API Key and API Secret inside Settings > API Key.

Once you have your API Key and Secret, you can test the authentication by sending GET request to /ping endpoint.

##### Request

```bash
curl -X GET "https://{{ API_BASE_PATH }}/ping" \
    -u YOUR_API_KEY:YOUR_API_SECRET \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

If your request is successful, you will get `pong` message as the output.

##### Response

```json
{
	"message": "pong"
}
```

##### HTTP Response

We are using standard HTTP response codes that is familiar to API development.

<div class="table-responsive">
<table class="table">
    <thead>
        <tr>
            <th>HTTP Code</th>
            <th>Explanation</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>200</td>
            <td>Ok (Usually you will get this code)</td>
        </tr>
        <tr>
            <td>401</td>
            <td>Authentication failed. This is due to wrong API Key and/or API Secret</td>
        </tr>
        <tr>
            <td>403</td>
            <td>Forbidden request</td>
        </tr>
        <tr>
            <td>422</td>
            <td>Unprocessable Entity. You will expect this HTTP Code if you send incomplete data for POST / PATCH request</td>
        </tr>
        <tr>
            <td>404</td>
            <td>Not found. The resources you are trying to reach is unavailable</td>
        </tr>
        <tr>
            <td>429</td>
            <td>Too Many Request. You will get this HTTP Code if you have exceeded our rate limits</td>
        </tr>
        <tr>
            <td>500, 502, 503, 504</td>
            <td>Server Errors. If you get this error, it is coming from our side. Just make sure you know how to handle it.</td>
        </tr>
    </tbody>
</table>
</div>

Each HTTP code output may carry some message. You may use the message to output to your application that you are building.

#### Rate Limit

A rate limit is set for every endpoint to allow fair usage for each users.

The rate limit is **60 requests/minute**. All request to our endpoint are count. **60 requests/minute** limit will be priced to fair usage when we have announced a paid API model.

Additionally, we send `X-RateLimit-Limit` and `X-RateLimit-Remaining` header for each requests.

Once you have exceeded the rate limit, you will receive `429` HTTP Status Code indicating **Too Many Requests**.

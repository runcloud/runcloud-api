<div class="row">
<div class="col col-xl-6">

#### List server logs

###### Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>search</span>
                    <span class="text-danger">optional</span>
                </td>
                <td>String</td>
                <td>Searching string if you wanted to search from list of logs</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/logs`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/logs
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/logs \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
  "data": [
    {
      "kind": "Info",
      "content": "Unban IP Address 1.1.1.222 from SSH",
      "created_at": "2019-06-27 13:09:35"
    },
    {
      "kind": "Info",
      "content": "Unban IP Address 1.1.1.1 from SSH",
      "created_at": "2019-06-27 13:08:10"
    },
    {
      "kind": "High Priority Info",
      "content": "Successfully deployed firewall rules",
      "created_at": "2019-06-27 10:16:21"
    },
    ....
    ....
    ....
  ],
  "meta": {
    "pagination": {
      "total": 1304,
      "count": 15,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 87,
      "links": {
        "next": "https:\/\/runcloud-gateway.test\/api\/v2\/servers\/7\/logs?page=2"
      }
    }
  }
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Create a firewall rule

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>type</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"global" or "rich" firewall rule</td>
        </tr>
        <tr>
            <td>
                <span>port</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer | String</td>
            <td>Port number or range of port number. E.g: 8000-8999</td>
        </tr>
        <tr>
            <td>
                <span>protocol</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"tcp" or "udp"</td>
        </tr>
        <tr>
            <td>
                <span>ipAddress</span>
                <span>Only if type is "rich"</span>
            </td>
            <td>String</td>
            <td>IP Address or CIDR</td>
        </tr>
        <tr>
            <td>
                <span>firewallAction</span>
                <span>Only if type is "rich"</span>
            </td>
            <td>String</td>
            <td>"accept" or "reject"</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/security/firewalls`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/security/firewalls
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/security/firewalls \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "type": "rich",
    "port": 9000,
    "protocol": "tcp",
    "ipAddress": "192.168.43.0/24",
    "firewallAction": "accept"
}'
```

###### Response

```json
{
	"id": 200,
	"type": "rich",
	"port": "9000",
	"protocol": "tcp",
	"ipAddress": "192.168.43.0/24",
	"firewallAction": "accept",
	"created_at": "2019-06-27 09:55:35"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List firewall rules

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/security/firewalls`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/security/firewalls
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/security/firewalls \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 4,
			"type": "global",
			"port": "22",
			"protocol": "tcp",
			"ipAddress": null,
			"firewallAction": null,
			"created_at": "2019-02-17 01:12:15"
		},
		{
			"id": 6,
			"type": "global",
			"port": "443",
			"protocol": "tcp",
			"ipAddress": null,
			"firewallAction": null,
			"created_at": "2019-02-17 01:12:15"
		},
		{
			"id": 5,
			"type": "global",
			"port": "80",
			"protocol": "tcp",
			"ipAddress": null,
			"firewallAction": null,
			"created_at": "2019-02-17 01:12:15"
		},
		{
			"id": 200,
			"type": "rich",
			"port": "9000",
			"protocol": "tcp",
			"ipAddress": "192.168.43.0/24",
			"firewallAction": "accept",
			"created_at": "2019-06-27 09:55:35"
		}
	],
	"meta": {
		"pagination": {
			"total": 4,
			"count": 4,
			"per_page": 15,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Firewall object

</div>
<div class="col col-xl-6 dark">

`PUT /servers/{serverId}/security/firewalls/{firewallId}`

###### HTTP Request

```
PUT https://{{ API_BASE_PATH }}/servers/{serverId}/security/firewalls/{firewallId}
```

###### Request

```bash
curl --request GET \
    --url https://{{ API_BASE_PATH }}/servers/7/security/firewalls/202 \
    -u YOUR_API_KEY:YOUR_API_SECRET \
    --header 'accept: application/json' \
    --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 202,
	"type": "rich",
	"port": "9000",
	"protocol": "tcp",
	"ipAddress": "192.168.43.0/24",
	"firewallAction": "accept",
	"created_at": "2019-06-27 10:15:07"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Deploy rules to server

</div>
<div class="col col-xl-6 dark">

`PUT /servers/{serverId}/security/firewalls`

###### HTTP Request

```
PUT https://{{ API_BASE_PATH }}/servers/{serverId}/security/firewalls
```

###### Request

```bash
curl --request PUT \
  --url https://{{ API_BASE_PATH }}/servers/7/security/firewalls \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json

```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete rule

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/security/firewalls/{firewallId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/security/firewalls/{firewallId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}//servers/7/security/firewalls/200 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 200,
	"type": "rich",
	"port": "9000",
	"protocol": "tcp",
	"ipAddress": "192.168.43.0/24",
	"firewallAction": "accept",
	"created_at": "2019-06-27 09:55:35"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List Fail2Ban blocked IP Addresses

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/security/fail2ban/blockedip`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/security/fail2ban/blockedip
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/security/fail2ban/blockedip \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
["1.1.1.3", "1.1.1.2", "1.1.1.1"]
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete blocked IP Address

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>ip</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>The blocked IP address you wanted to remove</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/security/fail2ban/blockedip`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/security/fail2ban/blockedip
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/security/fail2ban/blockedip \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "ip": "1.1.1.1"
}'
```

###### Response

```json
{
	"ip": "1.1.1.1"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

</div>
</div>

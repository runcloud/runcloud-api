<div class="row">
<div class="col col-xl-6">

#### Create a server

Connect a new server.

###### Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>name</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Name of the server</td>
            </tr>
            <tr>
                <td>
                    <span>ipAddress</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Public IP Address of the server</td>
            </tr>
            <tr>
                <td>
                    <span>provider</span>
                    <span>optional</span>
                </td>
                <td>String</td>
                <td>The provider of server. E.g: Digital Ocean, Linode, Vultr, AWS, etc</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "name": "My First Server",
    "ipAddress": "127.0.0.1",
    "provider": "Localhost"
}'
```

###### Response

```json
{
	"id": 65,
	"name": "My First Server",
	"provider": "Localhost",
	"ipAddress": "127.0.0.1",
	"os": "Ubuntu",
	"osVersion": null,
	"connected": false,
	"online": false,
	"agentVersion": null,
	"phpCLIVersion": "php73rc",
	"softwareUpdate": false,
	"securityUpdate": true,
	"transferStatus": "AVAILABLE",
	"created_at": "2019-06-20 18:10:23"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List servers

###### Query String Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>search</span>
                    <span>optional</span>
                </td>
                <td>Search string</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers
```

###### Request

```bash
curl --request GET \
  --url "https://{{ API_BASE_PATH }}/servers?page=1" \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 65,
			"name": "My First Server",
			"provider": "Localhost",
			"ipAddress": "127.0.0.1",
			"os": "Ubuntu",
			"osVersion": null,
			"connected": false,
			"online": false,
			"agentVersion": null,
			"phpCLIVersion": "php73rc",
			"softwareUpdate": false,
			"securityUpdate": true,
			"transferStatus": "AVAILABLE",
			"created_at": "2019-06-20 18:10:23"
		},
		{
			"id": 7,
			"name": "RunCloud 2 1604",
			"provider": "VMWare Fusion 10",
			"ipAddress": "192.168.43.210",
			"os": "Ubuntu",
			"osVersion": "xenial",
			"connected": true,
			"online": true,
			"agentVersion": "2.1.7-1+ubuntu16.04+2",
			"phpCLIVersion": "php56rc",
			"softwareUpdate": false,
			"securityUpdate": true,
			"transferStatus": "AVAILABLE",
			"created_at": "2019-01-16 02:03:14"
		}
	],
	"meta": {
		"pagination": {
			"total": 2,
			"count": 2,
			"per_page": 15,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List shared servers

###### Query string

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>search</span>
                    <span>optional</span>
                </td>
                <td>Search string</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/shared
```

###### Request

```bash
curl --request GET \
  --url "https://{{ API_BASE_PATH }}/servers/shared?page=1" \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 55,
			"name": "Test 2",
			"provider": "DigitalOcean",
			"ipAddress": "1.1.1.2",
			"os": "Ubuntu",
			"osVersion": null,
			"connected": true,
			"online": false,
			"agentVersion": "2.1.7-1+ubuntu16.04+1",
			"phpCLIVersion": "php73rc",
			"softwareUpdate": false,
			"securityUpdate": true,
			"transferStatus": "AVAILABLE",
			"created_at": "2019-05-19 05:13:13"
		}
	],
	"meta": {
		"pagination": {
			"total": 1,
			"count": 1,
			"per_page": 16,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Server object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{id}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{id}
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/65 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 65,
	"name": "My First Server",
	"provider": "Localhost",
	"ipAddress": "127.0.0.1",
	"os": "Ubuntu",
	"osVersion": null,
	"connected": false,
	"online": false,
	"agentVersion": null,
	"phpCLIVersion": "php73rc",
	"softwareUpdate": false,
	"securityUpdate": true,
	"transferStatus": "AVAILABLE",
	"created_at": "2019-06-20 18:10:23"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Installation Script

</div>
<div class="col col-xl-6 dark">

`GET /servers/{id}/installationscript`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{id}/installationscript
```

###### Request

```bash
  curl --request GET \
    --url https://{{ API_BASE_PATH }}/servers/65/installationscript \
    -u YOUR_API_KEY:YOUR_API_SECRET \
    --header 'accept: application/json' \
    --header 'content-type: application/json'
```

###### Response

```json
{
	"script": "export DEBIAN_FRONTEND=noninteractive; echo 'Acquire::ForceIPv4 \"true\";' | tee /etc/apt/apt.conf.d/99force-ipv4; apt-get update; apt-get install curl netcat-openbsd -y; curl --silent --location https://manage.runcloud.io/scripts/installer/cix510RbawOVExZANlzgUqcU9E1561024841dzRy0jIa3F0d7ibg4J94xSl1yB0dDBy0IJrIYMkjX2lNfu8JOY8gWbGB4tMWUwsw/d5fUlLVNYMvhm3c7LXGnTFgYw6KzN1qHNMc3ibafsyRWWICkGZseQ6OFtzARZiULAYTRwf9maKraHdtBCgZWgiCNNZx9Tqx8HVTL2XVivy56kD4MrHImxJJujeGNQCIk | bash -; export DEBIAN_FRONTEND=newt"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Server Stats

</div>
<div class="col col-xl-6 dark">

`GET /servers/{id}/stats`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{id}/stats
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/stats \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"stats": {
		"webApplication": 5,
		"database": 3,
		"cronJob": 1,
		"supervisor": 0
	},
	"geoRecord": {
		"country": "Singapore",
		"subdivision": "Unknown",
		"latitude": 1.2929,
		"longitude": 103.8547
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Hardware Info

</div>
<div class="col col-xl-6 dark">

`GET /servers/{id}/hardwareinfo`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{id}/hardwareinfo
```

###### Request

```bash
curl --request GET \
    --url https://{{ API_BASE_PATH }}/servers/7/hardwareinfo \
    -u YOUR_API_KEY:YOUR_API_SECRET \
    --header 'accept: application/json' \
    --header 'content-type: application/json'
```

###### Response

```json
{
	"kernelVersion": "4.4.0-87-generic",
	"processorName": "Intel(R) Core(TM) i7-7700K CPU @ 4.20GHz",
	"totalCPUCore": 2,
	"totalMemory": 0.9520301818847656,
	"freeMemory": 0.3449363708496094,
	"diskTotal": 9.376751616,
	"diskFree": 5.573582848,
	"loadAvg": 0,
	"uptime": "74h 54m 7s"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List available PHP version

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/php/version`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/php/version
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/php/version \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
["php55rc", "php56rc", "php70rc", "php71rc", "php72rc", "php73rc"]
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Change PHP-CLI version

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>phpVersion</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>PHP version you want to use for the CLI of this server/td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/php/cli`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/php/cli
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/php/cli \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "phpVersion":  "php72rc"
}'
```

###### Response

```json
{
	"id": 7,
	"name": "RunCloud Xenial Test",
	"provider": "VMWare Fusion 11",
	"ipAddress": "192.168.43.210",
	"os": "Ubuntu",
	"osVersion": "xenial",
	"connected": true,
	"online": true,
	"agentVersion": "2.1.7-1+ubuntu16.04+2",
	"phpCLIVersion": "php72rc",
	"softwareUpdate": false,
	"securityUpdate": true,
	"transferStatus": "AVAILABLE",
	"created_at": "2019-01-16 02:03:14"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Update Meta Data

###### Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>name</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Name of the server</td>
            </tr>
            <tr>
                <td>
                    <span>provider</span>
                    <span>optional</span>
                </td>
                <td>String</td>
                <td>Name of the company that provides your server</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{id}/settings/meta`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{id}/settings/meta
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/settings/meta \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "name": "RunCloud Xenial Test",
    "provider": "VMWare Fusion 11"
}'
```

###### Response

```json
{
	"id": 7,
	"name": "RunCloud Xenial Test",
	"provider": "VMWare Fusion 11",
	"ipAddress": "192.168.43.210",
	"os": "Ubuntu",
	"osVersion": "xenial",
	"connected": true,
	"online": true,
	"agentVersion": "2.1.7-1+ubuntu16.04+2",
	"phpCLIVersion": "php56rc",
	"softwareUpdate": false,
	"securityUpdate": true,
	"transferStatus": "AVAILABLE",
	"created_at": "2019-01-16 02:03:14"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Get SSH configuration

</div>
<div class="col col-xl-6 dark">

`GET /servers/{id}/settings/ssh`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{id}/settings/ssh
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/settings/ssh \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"passwordlessLogin": false,
	"useDns": false,
	"preventRootLogin": false
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Update SSH configuration

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>passwordlessLogin</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Only allow SSH using SSH Key</td>
        </tr>
        <tr>
            <td>
                <span>useDns</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Check reverse DNS of connecting machine</td>
        </tr>
        <tr>
            <td>
                <span>preventRootLogin</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Disable SSH login as "root"</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{id}/settings/ssh`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{id}/settings/ssh
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/settings/ssh \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "passwordlessLogin": true,
    "useDns": false,
    "preventRootLogin": true
}'
```

###### Response

```json
{
	"passwordlessLogin": true,
	"useDns": false,
	"preventRootLogin": true
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Software update

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>softwareUpdate</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Enable update for installed software</td>
        </tr>
        <tr>
            <td>
                <span>securityUpdate</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Enable update for security package</td>
        </tr>
    </tbody>

</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{id}/settings/autoupdate`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{id}/settings/autoupdate
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/settings/autoupdate \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "softwareUpdate": false,
    "securityUpdate": true
}'
```

###### Response

```json
{
	"id": 7,
	"name": "RunCloud Xenial Test",
	"provider": "VMWare Fusion 11",
	"ipAddress": "192.168.43.210",
	"os": "Ubuntu",
	"osVersion": "xenial",
	"connected": true,
	"online": true,
	"agentVersion": "2.1.7-1+ubuntu16.04+2",
	"phpCLIVersion": "php56rc",
	"softwareUpdate": false,
	"securityUpdate": true,
	"transferStatus": "AVAILABLE",
	"created_at": "2019-01-16 02:03:14"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete server

Deletes existing server.

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{id}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/65
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/65 \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  -u YOUR_API_KEY:YOUR_API_SECRET
```

###### Response

```json
{
	"id": 65,
	"name": "My First Server",
	"provider": "Localhost",
	"ipAddress": "127.0.0.1",
	"os": "Ubuntu",
	"osVersion": null,
	"connected": false,
	"online": false,
	"agentVersion": null,
	"transferStatus": "AVAILABLE",
	"created_at": "2019-06-20 10:00:41"
}
```

</div>
</div>

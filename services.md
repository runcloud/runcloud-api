<div class="row">
<div class="col col-xl-6">

#### List services

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/services`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/services
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/services \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"beanstalk": {
		"realName": "beanstalkd",
		"name": "Beanstalk",
		"memory": "1.2 MB",
		"cpu": "0.0000%",
		"running": true,
		"version": "1.10-3"
	},
	"httpd": {
		"realName": "apache2-rc",
		"name": "HTTPD/Apache",
		"memory": "30 MB",
		"cpu": "0.0024%",
		"running": true,
		"version": "2.4.39-1+ubuntu16.04+1"
	},
	"mariadb": {
		"realName": "mysql",
		"name": "MariaDB",
		"memory": "50 MB",
		"cpu": "0.0939%",
		"running": true,
		"version": "10.2.22+maria~xenial"
	},
	"memcached": {
		"realName": "memcached",
		"name": "Memcached",
		"memory": "Service stopped",
		"cpu": "Service stopped",
		"running": false,
		"version": "1.4.25-2ubuntu1.4"
	},
	"nginx": {
		"realName": "nginx-rc",
		"name": "NGiNX",
		"memory": "24 MB",
		"cpu": "0.0003%",
		"running": true,
		"version": "1.15.10-1+ubuntu16.04+1"
	},
	"redis": {
		"realName": "redis-server",
		"name": "Redis",
		"memory": "4.2 MB",
		"cpu": "0.0557%",
		"running": true,
		"version": "2:3.0.6-1ubuntu0.3"
	},
	"supervisor": {
		"realName": "supervisord",
		"name": "Supervisor",
		"memory": "Service stopped",
		"cpu": "Service stopped",
		"running": false,
		"version": "3.3.5"
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Trigger systemctl command

###### Parameters

<div calass="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>action</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>The action will be one of these: start,stop,restart,reload</td>
            </tr>
            <tr>
                <td>
                    <span>realName</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>You can get this from service.realName. E.g: redis-server</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/services`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/services
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/services \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "action": "start",
    "realName": "apache2-rc"
}'
```

###### Response

```json
{
	"action": "start",
	"realName": "apache2-rc",
	"name": "apache2-rc"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Add ssh key

###### Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>label</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Label of the credential</td>
            </tr>
            <tr>
                <td>
                    <span>username</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Username of user inside your server</td>
            </tr>
            <tr>
                <td>
                    <span>publicKey</span>
                    <span class="text-danger">required</span>                
                </td>
                <td>String</td>
                <td>The SSH Key string</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/sshcredentials`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/sshcredentials
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}servers/7/sshcredentials \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "label": "mypc-runcloud",
    "username": "runcloud",
    "publicKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjUkPY+cu9I25/3nH96vv6gwqKaKufohfQDv7sO5Z9UITba1vzeIMR1OGJaWgSY08ey9XTP/907hf/gfG0gP3AfrBd+bAuFoMc1W/NEQ5KXrfvGVX9VPR6A4or227NUDJ8NYeYigtsvCe8lMA8E19PUnUPQtl5mYSkN6lNZaxVWcMxhNrcHmvBcLHwxBsTGxn2DWYxBt+DMp/BirxYVInvd6kyY/a4Z6GKQ5iUAgjVyS/e683UX8bkNPN+s3Th20R+/ffvf93BRtwQQiw6KKa9OeWgFo4pdaNGE326RGwueE7bC7W2+vrG77ZG5eBx/R9Wvf/XJCUSGSHISo9lwQ69 puterisabrina@runcloud.io"
}'
```

###### Response

```json
{
	"id": 17,
	"label": "mypc-runcloud",
	"user_id": 2,
	"publicKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjUkPY+cu9I25/3nH96vv6gwqKaKufohfQDv7sO5Z9UITba1vzeIMR1OGJaWgSY08ey9XTP/907hf/gfG0gP3AfrBd+bAuFoMc1W/NEQ5KXrfvGVX9VPR6A4or227NUDJ8NYeYigtsvCe8lMA8E19PUnUPQtl5mYSkN6lNZaxVWcMxhNrcHmvBcLHwxBsTGxn2DWYxBt+DMp/BirxYVInvd6kyY/a4Z6GKQ5iUAgjVyS/e683UX8bkNPN+s3Th20R+/ffvf93BRtwQQiw6KKa9OeWgFo4pdaNGE326RGwueE7bC7W2+vrG77ZG5eBx/R9Wvf/XJCUSGSHISo9lwQ69 puterisabrina@runcloud.io",
	"created_at": "2019-06-22 14:50:22"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List SSH keys

###### Query String Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>label</span>
                    <span>optional</span>
                </td>
                <td>String</td>
                <td>Search string to search the label or the server user</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/sshcredentials`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/sshcredentials
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/sshcredentials \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 17,
			"label": "mypc-runcloud",
			"user_id": 2,
			"publicKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjUkPY+cu9I25/3nH96vv6gwqKaKufohfQDv7sO5Z9UITba1vzeIMR1OGJaWgSY08ey9XTP/907hf/gfG0gP3AfrBd+bAuFoMc1W/NEQ5KXrfvGVX9VPR6A4or227NUDJ8NYeYigtsvCe8lMA8E19PUnUPQtl5mYSkN6lNZaxVWcMxhNrcHmvBcLHwxBsTGxn2DWYxBt+DMp/BirxYVInvd6kyY/a4Z6GKQ5iUAgjVyS/e683UX8bkNPN+s3Th20R+/ffvf93BRtwQQiw6KKa9OeWgFo4pdaNGE326RGwueE7bC7W2+vrG77ZG5eBx/R9Wvf/XJCUSGSHISo9lwQ69 puterisabrina@runcloud.io",
			"created_at": "2019-06-22 14:50:22"
		},
		{
			"id": 14,
			"label": "test",
			"user_id": 2,
			"publicKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjUkPY+cu9I25/3nH96vv6gwqKaKufohfQDv7sO5Z9UITba1vzeIMR1OGJaWgSY08ey9XTP/907hf/gfG0gP3AfrBd+bAuFoMc1W/NEQ5KXrfvGVX9VPR6A4or227NUDJ8NYeYigtsvCe8lMA8E19PUnUPQtl5mYSkN6lNZaxVWcMxhNrcHmvBcLHwxBsTGxn2DWYxBt+DMp/BirxYVInvd6kyY/a4Z6GKQ5iUAgjVyS/e683UX8bkNPN+s3Th20R+/ffvf93BRtwQQiw6KKa9OeWgFo4pdaNGE326RGwueE7bC7W2+vrG77ZG5eBx/R9Wvf/XJCUSGSHISo9lwQ69 puterisabrina@runcloud.io",
			"created_at": "2019-06-22 14:38:52"
		}
	],
	"meta": {
		"pagination": {
			"total": 2,
			"count": 2,
			"per_page": 15,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSH Key object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/sshcredentials/{credentialId}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/sshcredentials/{credentialId}
```

###### Request

```bash
curl -X GET "https://{{ API_BASE_PATH }}/servers/yO4VAbX2BkNz/sshcredentials/OGv82zb25XLm" \
    -u YOUR_API_KEY:YOUR_API_SECRET \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"


curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/sshcredentials/17 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 17,
	"label": "mypc-runcloud",
	"user_id": 2,
	"publicKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjUkPY+cu9I25/3nH96vv6gwqKaKufohfQDv7sO5Z9UITba1vzeIMR1OGJaWgSY08ey9XTP/907hf/gfG0gP3AfrBd+bAuFoMc1W/NEQ5KXrfvGVX9VPR6A4or227NUDJ8NYeYigtsvCe8lMA8E19PUnUPQtl5mYSkN6lNZaxVWcMxhNrcHmvBcLHwxBsTGxn2DWYxBt+DMp/BirxYVInvd6kyY/a4Z6GKQ5iUAgjVyS/e683UX8bkNPN+s3Th20R+/ffvf93BRtwQQiw6KKa9OeWgFo4pdaNGE326RGwueE7bC7W2+vrG77ZG5eBx/R9Wvf/XJCUSGSHISo9lwQ69 puterisabrina@runcloud.io",
	"created_at": "2019-06-22 14:50:22"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete SSH key

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/sshcredentials/{credentialId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/sshcredentials/{credentialId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/sshcredentials/17 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 17,
	"label": "mypc-runcloud",
	"user_id": 2,
	"publicKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjUkPY+cu9I25/3nH96vv6gwqKaKufohfQDv7sO5Z9UITba1vzeIMR1OGJaWgSY08ey9XTP/907hf/gfG0gP3AfrBd+bAuFoMc1W/NEQ5KXrfvGVX9VPR6A4or227NUDJ8NYeYigtsvCe8lMA8E19PUnUPQtl5mYSkN6lNZaxVWcMxhNrcHmvBcLHwxBsTGxn2DWYxBt+DMp/BirxYVInvd6kyY/a4Z6GKQ5iUAgjVyS/e683UX8bkNPN+s3Th20R+/ffvf93BRtwQQiw6KKa9OeWgFo4pdaNGE326RGwueE7bC7W2+vrG77ZG5eBx/R9Wvf/XJCUSGSHISo9lwQ69 puterisabrina@runcloud.io",
	"created_at": "2019-06-22 14:50:22"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Database collations

</div>
<div class="col col-xl-6 dark">

`GET /static/databases/collations`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/static/databases/collations
```

###### Request

```bash
curl --request GET \
	--url https://{{ API_BASE_PATH }}/static/databases/collations \
	-u YOUR_API_KEY:YOUR_API_SECRET \
	--header 'accept: application/json' \
	--header 'content-type: application/json'
```

###### Response

```json
[
  "armscii8_bin",
  "armscii8_general_ci",
  "ascii_bin",
  "ascii_general_ci",
  "big5_bin",
  "big5_chinese_ci",
  "binary",
  ...
]
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Timezone

</div>
<div class="col col-xl-6 dark">

`GET /static/timezones`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/static/timezones
```

###### Request

```bash
curl --request GET \
	--url https://{{ API_BASE_PATH }}/static/timezones \
	-u YOUR_API_KEY:YOUR_API_SECRET \
	--header 'accept: application/json' \
	--header 'content-type: application/json'
```

###### Response

```json
[
  "Africa\/Abidjan",
  "Africa\/Accra",
  "Africa\/Addis_Ababa",
  "Africa\/Algiers",
  "Africa\/Asmara",
  "Africa\/Bamako",
  "Africa\/Bangui",
  "Africa\/Banjul",
  "Africa\/Bissau",
  "Africa\/Blantyre",
  "Africa\/Brazzaville",
  ...
  ...
  ...
]
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Available script installer

</div>
<div class="col col-xl-6 dark">

`GET /static/webapps/installers`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/static/webapps/installers
```

###### Request

```bash
curl -X GET "https://{{ API_BASE_PATH }}/static/installers" \
    -u YOUR_API_KEY:YOUR_API_SECRET \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"

curl --request GET \
  --url https://{{ API_BASE_PATH }}/static/webapps/installers \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
[
	"concrete5",
	"drupal",
	"grav",
	"gravadmin",
	"joomla",
	"myBB",
	"phpBB",
	"phpMyAdmin",
	"piwik",
	"prestaShop",
	"wordpress"
]
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Available SSL Protocols

###### Parameters

<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <span>webServer</span>
                    <span class="text-danger">required</span>
                </td>
                <td>String</td>
                <td>Type of web server. Currently only support "nginx"</td>
            </tr>
        </tbody>
    </table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /static/ssl/protocols`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/static/ssl/protocols
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/static/ssl/protocols \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "webServer": "nginx"
  }'
```

###### Response

```json
[
	{
		"id": 1,
		"protocol": "TLSv1.1 TLSv1.2 TLSv1.3"
	},
	{
		"id": 2,
		"protocol": "TLSv1.2 TLSv1.3"
	},
	{
		"id": 3,
		"protocol": "TLSv1.3"
	}
]
```

</div>
</div>

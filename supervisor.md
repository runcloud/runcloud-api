<div class="row">
<div class="col col-xl-6">

#### Available binary

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/supervisors/binaries`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/supervisors/binaries
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/supervisors/binaries \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
[
	"",
	"/RunCloud/Packages/php55rc/bin/php",
	"/RunCloud/Packages/php56rc/bin/php",
	"/RunCloud/Packages/php70rc/bin/php",
	"/RunCloud/Packages/php71rc/bin/php",
	"/RunCloud/Packages/php72rc/bin/php",
	"/RunCloud/Packages/php73rc/bin/php",
	"/usr/bin/node"
]
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Create job

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>label</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Label of the job</td>
        </tr>
        <tr>
            <td>
                <span>username</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Linux System User that will be running this job</td>
        </tr>
        <tr>
            <td>
                <span>autoRestart</span>
                <span>optional</span>
            </td>
            <td>Boolean</td>
            <td>Auto restart if job failed</td>
        </tr>
        <tr>
            <td>
                <span>autoStart</span>
                <span>optional</span>
            </td>
            <td>Boolean</td>
            <td>Auto start once supervisors start running</td>
        </tr>
        <tr>
            <td>
                <span>numprocs</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Number of background job to run for this job</td>
        </tr>
        <tr>
            <td>
                <span>binary</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Use one of the available binary selection</td>
        </tr>
        <tr>
            <td>
                <span>directory</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Directory which you wanted supervisor job to start</td>
        </tr>
        <tr>
            <td>
                <span>command</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Command you wanted to run</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/supervisors`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/supervisors
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/supervisors \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "label": "my-first-job",
    "username": "runcloud",
    "numprocs": 5,
    "autoStart": true,
    "autoRestart": true,
    "binary": "/RunCloud/Packages/php73rc/bin/php",
    "command": "/home/runcloud/queue.php"
}'
```

###### Response

```json
{
	"id": 12,
	"label": "my-first-job",
	"username": "runcloud",
	"numprocs": 5,
	"autoStart": true,
	"autoRestart": true,
	"directory": "/",
	"command": "/RunCloud/Packages/php73rc/bin/php /home/runcloud/queue.php",
	"created_at": "2019-06-22 09:35:01"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List jobs

List all supervisor jobs.

###### Query String Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>search</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Search job label</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/supervisors`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/supervisors
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/supervisors \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 12,
			"label": "my-first-job",
			"username": "runcloud",
			"numprocs": 5,
			"autoStart": true,
			"autoRestart": true,
			"directory": "/",
			"command": "/RunCloud/Packages/php73rc/bin/php /home/runcloud/queue.php",
			"created_at": "2019-06-22 09:35:01"
		}
	],
	"meta": {
		"pagination": {
			"total": 1,
			"count": 1,
			"per_page": 15,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List jobs status

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/supervisors/status`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/supervisors/status
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/supervisors/status \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"my-first-job": [
		{
			"process": "my-first-job_0",
			"status": "RUNNING",
			"pid": "35200",
			"uptime": "0:00:36"
		},
		{
			"process": "my-first-job_1",
			"status": "RUNNING",
			"pid": "35201",
			"uptime": "0:00:36"
		},
		{
			"process": "my-first-job_2",
			"status": "RUNNING",
			"pid": "35198",
			"uptime": "0:00:36"
		},
		{
			"process": "my-first-job_3",
			"status": "RUNNING",
			"pid": "35199",
			"uptime": "0:00:36"
		},
		{
			"process": "my-first-job_4",
			"status": "RUNNING",
			"pid": "35197",
			"uptime": "0:00:36"
		}
	]
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Rebuild jobs

Rebuild all supervisor jobs.

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/supervisors/rebuild`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/supervisors/rebuild
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/supervisors/rebuild \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json

```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Job object

Get supervisor job.

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/supervisors/{supervisorId}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/supervisors/{supervisorId}
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/supervisors/13 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 13,
	"label": "my-first-job",
	"username": "runcloud",
	"numprocs": 5,
	"autoStart": true,
	"autoRestart": true,
	"directory": "/",
	"command": "/RunCloud/Packages/php73rc/bin/php /home/runcloud/queue.php",
	"created_at": "2019-06-22 09:55:07"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Reload job

Reload a supervisor job.

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/supervisors/{supervisorId}/reload`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/supervisors/{supervisorId}/reload
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/supervisors/13/reload \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 13,
	"label": "my-first-job",
	"username": "runcloud",
	"numprocs": 5,
	"autoStart": true,
	"autoRestart": true,
	"directory": "/",
	"command": "/RunCloud/Packages/php73rc/bin/php /home/runcloud/queue.php",
	"created_at": "2019-06-22 09:55:07"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete job

Delete a job.

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/supervisors/{supervisorId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/supervisors/{supervisorId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/supervisors/13 \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 13,
	"label": "my-first-job",
	"username": "runcloud",
	"numprocs": 5,
	"autoStart": true,
	"autoRestart": true,
	"directory": "/",
	"command": "/RunCloud/Packages/php73rc/bin/php /home/runcloud/queue.php",
	"created_at": "2019-06-22 09:55:07"
}
```

</div>
</div>

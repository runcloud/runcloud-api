<div class="row">
<div class="col col-xl-6">

#### Create system user

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>username</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Username of the user that you wanted to create</td>
        </tr>
        <tr>
            <td>
                <span>password</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Password for the created user</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/users`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/users
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/users \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "username": "myuser",
    "password": "mypassword"
}'
```

###### Response

```json
{
	"id": 142,
	"username": "myuser",
	"deploymentKey": null,
	"deleteable": true,
	"created_at": "2019-06-22 02:19:59"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List system users

###### Query String Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>search</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Username of user that you wanted to search</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/users`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/users
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/users \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 2,
			"username": "runcloud",
			"deploymentKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1PmnpByqe68RbCYJNnUWR1cH4QqJ5A0UzQQ8df+W0TSvhEZqCdbDBNQ/+RAZPZ7f7kp9pkHIj7AbRhIFizeq5J7u4evf84DEbnGna41xNl18UveCqoeJl8PpMoAht+rXXRap7W/D1yQQKls/leKrXRbbDHVFRsS3HQ1am2+0EHY/CC3YOKzTcMgExN7O9vE/NSYsNPE9+m0rlQpCxNhkROB2Nwmt9Zrw9An/RRJuxrUL8tPEgT3UjugTc1Vf+UMv44W1k2LMAFGJYHSSwIIwVGYUECJ/OA5e0MBNxgNG4TMXd4zYyL3uN7/wCSap85HQpgxhMiiVnCmKJE2HOxFXB GIT Deployment Key for user runcloud.",
			"deleteable": false,
			"created_at": "2019-02-12 00:00:00"
		},
		{
			"id": 1,
			"username": "root",
			"deploymentKey": null,
			"deleteable": false,
			"created_at": "2019-02-12 00:00:00"
		},
		{
			"id": 142,
			"username": "myuser",
			"deploymentKey": null,
			"deleteable": true,
			"created_at": "2019-06-22 02:19:59"
		}
	],
	"meta": {
		"pagination": {
			"total": 3,
			"count": 3,
			"per_page": 15,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### System user object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/users/{userId}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/users/{userId}
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/users/142 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 142,
	"username": "myuser",
	"deploymentKey": null,
	"deleteable": true,
	"created_at": "2019-06-22 02:19:59"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Change password

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>password</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Password for user</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/users/{userId}/password`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/users/{userId}/password
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/users/142/password \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "password": "myupdatedpassword"
}'
```

###### Response

```json
{
	"id": 142,
	"username": "myuser",
	"deploymentKey": null,
	"deleteable": true,
	"created_at": "2019-06-22 02:19:59"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Generate GIT deployment key

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/users/{userId}/deploymentkey`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/users/{userId}/deploymentkey
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/users/142/deploymentkey \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 142,
	"username": "myuser",
	"deploymentKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDSUS6qaeZsFIqMCgQlm/Mrr7wrxc1FEG+NPHHXecmcQrLIOm0XpM4NcytrdQ85xAqIqqg9vYOmT+yuxMDD4EDS1VypYGkOgrcP3bjEX7avO+GZDctNqug6EuLN2PC0+rBXsgmUZfm87czwOPDnU0C22/QzG1vQe5tGthlimZGqbwDETLgKCc8z3ciEN+n6Qcd3MIc2UQbXZUBy0bRcgtJQR81Q/TM+66fFfHCB8CnLDPgo/LPIgymA375hYH+h7MBShfL85xP87HoU7w1XS2EDVQL9r2kteP1VNN5z+7ybMgmKrzz0tWcqFYK1f+Kh6jflQ7yMwXsu/wQPtusOR2rn GIT Deployment Key for user myuser.",
	"deleteable": true,
	"created_at": "2019-06-22 02:19:59"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete system user

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/users/{userId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/users/{userId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/users/143 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 143,
	"username": "myuser2",
	"deploymentKey": null,
	"deleteable": true,
	"created_at": "2019-06-22 03:43:15"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Create web application

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>name</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Name of the Web Application</td>
        </tr>
        <tr>
            <td>
                <span>domainName</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Domain name for the Web Application</td>
        </tr>
        <tr>
            <td>
                <span>user</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>The System User who will own this Web Application</td>
        </tr>
        <tr>
            <td>
                <span>publicPath</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Append a public path to a Web Application. If not defined, Web Application root path will be used as publicPath</td>
        </tr>
        <tr>
            <td>
                <span>phpVersion</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Refer to Server section on how to get list of PHP version supported</td>
        </tr>
        <tr>
            <td>
                <span>stack</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"hybrid", "nativenginx", or "customnginx"</td>
        </tr><tr>
            <td>
                <span>stackMode</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"production" or "development"</td>
        </tr>
        <tr>
            <td>
                <span>clickjackingProtection</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>xssProtection</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>mimeSniffingProtection</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManager</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"dynamic", "ondemand", or "static"</td>
        </tr>
        <tr>
            <td>
                <span>processManagerStartServers</span>
                <span>required if processManager is "dynamic"</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManagerMinSpareServers</span>
                <span>required if processManager is "dynamic"</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManagerMaxSpareServers</span>
                <span>required if processManager is "dynamic"</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManagerMaxChildren</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManagerMaxRequests</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>openBasedir</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>If you left this empty, open_basedir will be empty. To add a default value, use "/home/{systemUser.username}/webapps/{webApp.name}:/var/lib/php/session:/tmp"</td>
        </tr>
        <tr>
            <td>
                <span>timezone</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>You can get this from Static Data for Timezone</td>
        </tr>
        <tr>
            <td>
                <span>disableFunctions</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>If you left this empty, no php functions will be disabled. If you wanted to supply the default value, see the request example</td>
        </tr>
        <tr>
            <td>
                <span>maxExecutionTime</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>maxInputTime</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>maxInputVars</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>memoryLimit</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Supplied value is in MB.</td>
        </tr>
        <tr>
            <td>
                <span>postMaxSize</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Supplied value is in MB. Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>uploadMaxFilesize</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Supplied value is in MB. Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>sessionGcMaxlifetime</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Supplied value is in seconds</td>
        </tr>
        <tr>
            <td>
                <span>allowUrlFopen</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td></td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/webapps/custom`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/custom
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/custom \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "name": "testing",
  "domainName": "testing.com",
  "user": 142,
  "publicPath": null,
  "phpVersion": "php73rc",
  "stack": "hybrid",
  "stackMode": "production",
  "clickjackingProtection": true,
  "xssProtection": true,
  "mimeSniffingProtection": true,
  "processManager": "ondemand",
  "processManagerMaxChildren": 50,
  "processManagerMaxRequests": 500,
  "openBasedir": "/home/myuser/webapps/testing:/var/lib/php/session:/tmp",
  "timezone": "UTC",
  "disableFunctions": "getmyuid,passthru,leak,listen,diskfreespace,tmpfile,link,ignore_user_abort,shell_exec,dl,set_time_limit,exec,system,highlight_file,source,show_source,fpassthru,virtual,posix_ctermid,posix_getcwd,posix_getegid,posix_geteuid,posix_getgid,posix_getgrgid,posix_getgrnam,posix_getgroups,posix_getlogin,posix_getpgid,posix_getpgrp,posix_getpid,posix,_getppid,posix_getpwuid,posix_getrlimit,posix_getsid,posix_getuid,posix_isatty,posix_kill,posix_mkfifo,posix_setegid,posix_seteuid,posix_setgid,posix_setpgid,posix_setsid,posix_setuid,posix_times,posix_ttyname,posix_uname,proc_open,proc_close,proc_nice,proc_terminate,escapeshellcmd,ini_alter,popen,pcntl_exec,socket_accept,socket_bind,socket_clear_error,socket_close,socket_connect,symlink,posix_geteuid,ini_alter,socket_listen,socket_create_listen,socket_read,socket_create_pair,stream_socket_server",
  "maxExecutionTime": 30,
  "maxInputTime": 60,
  "maxInputVars": 1000,
  "memoryLimit": 256,
  "postMaxSize": 256,
  "uploadMaxFilesize": 256,
  "sessionGcMaxlifetime": 1440,
  "allowUrlFopen": true
}'
```

###### Response

```json
{
	"id": 109,
	"server_user_id": 142,
	"name": "testing",
	"rootPath": "/home/myuser/webapps/testing",
	"publicPath": "/home/myuser/webapps/testing",
	"phpVersion": "php73rc",
	"stack": "hybrid",
	"stackMode": "production",
	"type": "custom",
	"defaultApp": false,
	"alias": null,
	"pullKey1": "jwMZwtXP3ItQRKKoMSZboAXr1561748870",
	"pullKey2": "zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu",
	"created_at": "2019-06-28 19:07:50"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List web applications

###### Query String Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>search</span>
                <span>optional</span>
            </td>
            <td>Searching string if you wanted to search from list of Web Apps</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
  "data": [
    ...
    ...
    ...
    {
      "id": 109,
      "server_user_id": 142,
      "name": "testing",
      "rootPath": "\/home\/myuser\/webapps\/testing",
      "publicPath": "\/home\/myuser\/webapps\/testing",
      "phpVersion": "php73rc",
      "stack": "hybrid",
      "stackMode": "production",
      "type": "custom",
      "defaultApp": false,
      "alias": null,
      "pullKey1": "jwMZwtXP3ItQRKKoMSZboAXr1561748870",
      "pullKey2": "zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu",
      "created_at": "2019-06-28 19:07:50"
    }
  ],
  "meta": {
    "pagination": {
      "total": 6,
      "count": 6,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 1,
      "links": {}
    }
  }
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Web application object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 109,
	"server_user_id": 142,
	"name": "testing",
	"rootPath": "/home/myuser/webapps/testing",
	"publicPath": "/home/myuser/webapps/testing",
	"phpVersion": "php73rc",
	"stack": "hybrid",
	"stackMode": "production",
	"type": "custom",
	"defaultApp": false,
	"alias": null,
	"pullKey1": "jwMZwtXP3ItQRKKoMSZboAXr1561748870",
	"pullKey2": "zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu",
	"created_at": "2019-06-28 19:07:50"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Set as default

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/webapps/{webAppId}/default`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/default
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/default \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 109,
	"server_user_id": 142,
	"name": "testing",
	"rootPath": "/home/myuser/webapps/testing",
	"publicPath": "/home/myuser/webapps/testing",
	"phpVersion": "php73rc",
	"stack": "hybrid",
	"stackMode": "production",
	"type": "custom",
	"defaultApp": true,
	"alias": null,
	"pullKey1": "jwMZwtXP3ItQRKKoMSZboAXr1561748870",
	"pullKey2": "zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu",
	"created_at": "2019-06-28 19:07:50"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Remove from default

<div class="alert alert-success">
  The return value will get defaultApp = true. You need to GET the web application object to get the newest defaultApp value. The value will only change to false when our server has successfully contacted your server.
</div>

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/webapps/{webAppId}/default`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/default
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/default \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 109,
	"server_user_id": 142,
	"name": "testing",
	"rootPath": "/home/myuser/webapps/testing",
	"publicPath": "/home/myuser/webapps/testing",
	"phpVersion": "php73rc",
	"stack": "hybrid",
	"stackMode": "production",
	"type": "custom",
	"defaultApp": true,
	"alias": null,
	"pullKey1": "jwMZwtXP3ItQRKKoMSZboAXr1561748870",
	"pullKey2": "zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu",
	"created_at": "2019-06-28 19:07:50"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Rebuild web application

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/webapps/{webAppId}/rebuild`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/rebuild
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/rebuild \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 109,
	"server_user_id": 142,
	"name": "testing",
	"rootPath": "/home/myuser/webapps/testing",
	"publicPath": "/home/myuser/webapps/testing",
	"phpVersion": "php73rc",
	"stack": "hybrid",
	"stackMode": "production",
	"type": "custom",
	"defaultApp": false,
	"alias": null,
	"pullKey1": "jwMZwtXP3ItQRKKoMSZboAXr1561748870",
	"pullKey2": "zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu",
	"created_at": "2019-06-28 19:07:50"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Cloning GIT repository

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>provider</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"custom", "bitbucket", "github", "gitlab", "selfhostedgitlab"</td>
        </tr>
        <tr>
            <td>
                <span>repository</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Must use this format: &lt;username&gt;/&lt;repository&gt;</td>
        </tr>
        <tr>
            <td>
                <span>branch</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Repository branch to clone</td>
        </tr>
        <tr>
            <td>
                <span>gitUser</span>
                <span>required if provider is "custom" or "selfhostedgitlab"</span>
            </td>
            <td>String</td>
            <td>Username of git server user</td>
        </tr>
        <tr>
            <td>
                <span>gitHost</span>
                <span>required if provider is "custom" or "selfhostedgitlab"</span>
            </td>
            <td>String</td>
            <td>Hostname of your GIT server</td>
        </tr>
    </tbody>
</table>
</div>

<div class="alert alert-success">The timeout for cloning your script is around <strong>TWO MINUTES</strong></div>

<div class="alert alert-success">
  You can construct Webhook URL using this template: https://{{ PANEL_DOMAIN }}/webhooks/git/{webApp.pullKey1}/{webApp.pullKey2}. 
<br>
For example, https://{{ PANEL_DOMAIN }}/webhooks/git/jwMZwtXP3ItQRKKoMSZboAXr1561748870/zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/webapps/{webAppId}/git`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/git
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/git \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "provider": "github",
  "repository": "coolcodemy/test",
  "branch": "master"
}'
```

###### Response

```json
{
	"id": 37,
	"provider": "github",
	"gitHost": null,
	"gitUser": null,
	"branch": "master",
	"repositoryData": {
		"url": "https://github.com/coolcodemy/test",
		"repo": "git@github.com:coolcodemy/test.git"
	},
	"atomic": false,
	"atomic_project_id": null,
	"autoDeploy": false,
	"deployScript": null,
	"created_at": "2019-06-29 03:36:36"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### GIT Object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}/git`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/git
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/git \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 37,
	"provider": "github",
	"gitHost": null,
	"gitUser": null,
	"branch": "master",
	"repositoryData": {
		"url": "https://github.com/coolcodemy/test",
		"repo": "git@github.com:coolcodemy/test.git"
	},
	"atomic": false,
	"atomic_project_id": null,
	"autoDeploy": false,
	"deployScript": null,
	"created_at": "2019-06-29 03:36:36"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Change GIT branch

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>branch</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Available GIT branch for your repository</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/webapps/{webAppId}/git/{gitId}/branch`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/git/{gitId}/branch
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/git/37/branch \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "branch": "dev"
}'
```

###### Response

```json
{
	"id": 37,
	"provider": "github",
	"gitHost": null,
	"gitUser": null,
	"branch": "dev",
	"repositoryData": {
		"url": "https://github.com/coolcodemy/test",
		"repo": "git@github.com:coolcodemy/test.git"
	},
	"atomic": false,
	"atomic_project_id": null,
	"autoDeploy": false,
	"deployScript": null,
	"created_at": "2019-06-29 03:36:36"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Customize GIT deployment script

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>autoDeploy</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Enable deployment script</td>
        </tr>
        <tr>
            <td>
                <span>deployScript</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>The script that will be running when GIT Webhook URL got a hit</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/webapps/{webAppId}/git/{gitId}/script`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/git/{gitId}/script
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/git/37/script \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "autoDeploy": true,
  "deployScript": "git merge\nphp artisan migrate"
}'
```

###### Response

```json
{
	"id": 37,
	"provider": "github",
	"gitHost": null,
	"gitUser": null,
	"branch": "dev",
	"repositoryData": {
		"url": "https://github.com/coolcodemy/test",
		"repo": "git@github.com:coolcodemy/test.git"
	},
	"atomic": false,
	"atomic_project_id": null,
	"autoDeploy": true,
	"deployScript": "git merge\nphp artisan migrate",
	"created_at": "2019-06-29 03:36:36"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Force deployment using deployment script

</div>
<div class="col col-xl-6 dark">

`PUT /servers/{serverId}/webapps/{webAppId}/git/{gitId}/script`

###### HTTP Request

```
PUT https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/git/{gitId}/script
```

###### Request

```bash
curl --request PUT \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/git/37/script \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json

```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Remove GIT repository

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/webapps/{webAppId}/git/{gitId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/git/{gitId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/git/37 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 37,
	"provider": "github",
	"gitHost": null,
	"gitUser": null,
	"branch": "dev",
	"repositoryData": {
		"url": "https://github.com/coolcodemy/test",
		"repo": "git@github.com:coolcodemy/test.git"
	},
	"atomic": false,
	"atomic_project_id": null,
	"autoDeploy": true,
	"deployScript": "git merge\nphp artisan migrate",
	"created_at": "2019-06-29 03:36:36"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Install PHP Script

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>name</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Available options: "concrete5", "drupal", "grav", "gravadmin", "joomla", "myBB", "phpBB", "phpMyAdmin", "piwik", "prestaShop", "wordpress"</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/webapps/{webAppId}/installer`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/installer
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/installer \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "name": "wordpress"
}'
```

###### Response

```json
{
	"id": 2,
	"name": "wordpress",
	"realName": "WordPress",
	"created_at": "2019-06-29 15:22:57"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### PHP Script object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}/installer`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/installer
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/installer \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 2,
	"name": "wordpress",
	"realName": "WordPress",
	"created_at": "2019-06-29 15:22:57"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Remove PHP Script

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/webapps/{webAppId}/installer/{installerId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/installer/{installerId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/installer/2 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 2,
	"name": "wordpress",
	"realName": "WordPress",
	"created_at": "2019-06-29 15:22:57"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Add Domain Name

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>name</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>The domain name you would like to add</td>
        </tr>
        <tr>
            <td>
                <span>www</span>
                <span class="text-danger">optional</span>
            </td>
            <td>Boolean</td>
            <td>It's either "true" or "false"</td>
        </tr>
        <tr>
            <td>
                <span>redirection</span>
                <span class="text-danger">optional</span>
            </td>
            <td>String</td>
            <td>It could be "none", "www", or "non-www" </td>
        </tr>
        <tr>
            <td>
                <span>type</span>
                <span class="text-danger">optional</span>
            </td>
            <td>String</td>
            <td>Domain name could set as "alias" or "primary" or "redirect. Default value if it is the first domain was "primary" else "alias"</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/webapps/{webAppId}/domains`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/domains
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/domains \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "name"      : "*.testing.com",
    "www"       : true,
    "routeWww"  : false,
    "type"      : "primary"
}'
```

###### Response

```json
{
	"id": 11,
	"name": "*.testing.com",
	"type": "primary",
	"www": 1,
	"redirection": "none",
	"wildcard": 0,
	"dns_integration": "none",
	"status": null,
	"created_at": "2021-06-17 04:28:16"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### List Domain Names

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}/domains`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/domains
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/domains \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"data": [
		{
			"id": 128,
			"name": "*.testing.com",
			"type": "primary",
			"www": 0,
			"redirection": "non-www",
			"wildcard": 0,
			"dns_integration": "none",
			"status": null,
			"created_at": "2019-06-30 06:13:29"
		},
		{
			"id": 126,
			"name": "testing.com",
			"type": "primary",
			"www": 0,
			"redirection": "non-www",
			"wildcard": 0,
			"dns_integration": "none",
			"status": null,
			"created_at": "2019-06-28 19:07:50"
		}
	],
	"meta": {
		"pagination": {
			"total": 2,
			"count": 2,
			"per_page": 100,
			"current_page": 1,
			"total_pages": 1,
			"links": {}
		}
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Domain Name Object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}/domains/{domainId}`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/domains/{domainId}
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/domains/128 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 128,
	"name": "*.testing.com",
	"type": "primary",
	"www": 0,
	"redirection": "non-www",
	"wildcard": 0,
	"dns_integration": "none",
	"status": null,
	"created_at": "2019-06-30 06:13:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete Domain Name

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/webapps/{webAppId}/domains/{domainId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/domains/{domainId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/domains/128 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 128,
	"name": "*.testing.com",
	"type": "alias",
	"www": 0,
	"redirection": "none",
	"wildcard": 0,
	"dns_integration": "none",
	"status": null,
	"created_at": "2019-06-30 06:13:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Basic - Install SSL

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>provider</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"letsencrypt" , "custom" or "csr"</td>
        </tr>
        <tr>
            <td>
                <span>enableHttp</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Either "true" or "false"</td>
        </tr>
        <tr>
            <td>
                <span>enableHsts</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Either "true" or "false"</td>
        </tr>
        <tr>
            <td>
                <span>ssl_protocol_id</span>
                <span class="text-danger">Optional</span>
            </td>
            <td>Integer</td>
            <td>SSL Protocol. You can get this value from Static Data API. It will use latest possible value if not supplied.</td>
        </tr>
        <tr>
            <td>
                <span>authorizationMethod</span>
                <span>required if provider is "letsencrypt"</span>
            </td>
            <td>String</td>
            <td>"http-01" or "dns-01"</td>
        </tr>
        <tr>
            <td>
                <span>externalApi</span>
                <span>required if provider is "letsencrypt" and authorizationMethod is "dns-01"</span>
            </td>
            <td>Integer</td>
            <td>Id of the 3rd Party API to use. </td>
        </tr>
        <tr>
            <td>
                <span>environment</span>
                <span>required if provider is "letsencrypt"</span>
            </td>
            <td>String</td>
            <td>"live" or "staging" environment</td>
        </tr>
        <tr>
            <td>
                <span>privateKey</span>
                <span>required if provider is "custom"</span>
            </td>
            <td>RSA Private Key String</td>
            <td>SSL Private Key</td>
        </tr>
        <tr>
            <td>
                <span>certificate</span>
                <span>required if provider is "custom"</span>
            </td>
            <td>RSA Public Key String</td>
            <td>SSL Certificate</td>
        </tr>
        <tr>
            <td>
                <span>csrKeyType</span>
                <span>required if provider is "csr"</span>
            </td>
            <td>String</td>
            <td>"rsa-2048", "ecdsa-p384", "ecdsa-p256" and "rsa-4096"</td>
        </tr>
        <tr>
            <td>
                <span>organization</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your organization name, default value was RunCloud Organization</td>
        </tr>
        <tr>
            <td>
                <span>department</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your department name, default value was RunCloud Department</td>
        </tr>
        <tr>
            <td>
                <span>city</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your city name, default value for city where RunCloud Organization has been found</td>
        </tr>
        <tr>
            <td>
                <span>state</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your state name, default value for state where RunCloud Organization has been found</td>
        </tr>
        <tr>
            <td>
                <span>country</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your country name, default value for country where RunCloud Organization has been found</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/webapps/{webAppId}/ssl`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/ssl
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/ssl \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "provider": "custom",
    "enableHttp": false,
    "enableHsts": true,
    "ssl_protocol_id": 3,
    "privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDem6F7HLVxStq/\n/t6aThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0\nzkVT2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCg\nuVkSehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5am\nLU3iSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWro\nCEIQ61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvo\nrn+YPEvDAgMBAAECggEAXOcIqo+dra12w8ADeNRxx8LdW+IMaLOLz6euxbKLUzd9\nlQC0WIKf86jR66B7uljr6HVPsNOXb5rKt/wJ9OdTUW33nn3YKe/41j1VfnbD1pme\nHWFwGP+LpINt7EvzsFEYNDiwV7p+neUqVyljYhGtlBJI70+5dEg4ihBVuWolcQai\nH06XxdT72O41RKr/uSzKcuxZBQhdD9Y505UVMsHOjFu0HMUMZ/wk7msZg4nyYmfg\nW0h/LbzmzblbEzF+wd4PYTE5FC4SRiS1/E8aDe78Rg7ww7pxgW9OvGlMWv4lrfco\nBO430h+VBodQnHHaTj4lV59pYhQGvlHwkSDEHpuWAQKBgQD3Jh6sIY97kOCJ9oET\nlhsBZ6JICiqmsieWRuSmMoiErBezVZ9QS5C5GOL4GkPsc7Bw0s6RB+9TZsEPS3DH\nglpzr5a89d3BRlkSNFSSHeq/W8/s8DYvMoQeapYTr4ijH97y18IjChozx5fcmzQS\nY8J0hYOruabZgyjBvSABNxokwwKBgQDmlIR3Mj7YQsSyyvgOp0r0e1u2zNnnm5n6\nC/kZR1SuhVjQ2tdwPafqjdSoFimUMPrSmJBT6U7LqfEtRaByR9xEHR0d3qb8Ofni\nSngD4NWmPSX6GnWwJVsPHDSqn6JW1eEdAwMisB7F4FhRmvdffmIaVI89HZSuv5gl\n/uEQRGbNAQKBgH+ODQaJy6PaggiyUKvrLMTs17SWiqy+BfBpZljge9T9fL3x0ud+\nGJNvZLTn2WaPzuBr7HCtx7cjsUBTj0Fo5YYPeZzMyEaYKCBdIcjH6AAbQpTm5RA2\n4jlQiWRflAWczVRIRsoOzLcsrBQPhjB3jETXI73dc1+PcdmL4pi996BBAoGAEuob\n86srfJH9kK0VrB4NCAEWhOhI97bL6rcQuAIh8C8AGiHZiluEark3uJIY1w8thBj/\nveJllE9ceVo8zyMV7oB04v5gtFANL4LsVWUcIYbilqGVBd4KmjK2H0j5CCaDUN3u\nY+oOnCzLEeakZDD52y8UkO7cQ3l0AanmzG7QAAECgYBVTTgYqAoUG6wVuQw5Jrnz\n9/ItvU4zPKDadnoIRgMZq+wXo8f8dMSzNjlylv+vQF99E9NRMMIVbyeH4/q5ANGl\nkcFwSyAhE7z+uSV0Ho6OqZVqis8e9rcin9+opC5BePQAjgAyAYciXpE95YXQcYMM\nlXCUsdHQomIAwFN2TNWohQ==\n-----END PRIVATE KEY-----",
    "certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqACCQC6DH7qQzl7LTANBgkqhkiG9w0BAQsFADCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wHhcNMTgwMzAyMDMyNjI3WhcNMTkwMzAyMDMyNjI3WjCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDem6F7HLVxStq//t6a\nThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0zkVT\n2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCguVkS\nehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5amLU3i\nSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWroCEIQ\n61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvorn+Y\nPEvDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA+HxyKESRAzUmDt1aE0v6u95QBB\niGGhVszAviF6ZKaf6bov0Ozq+67Pq+3Tksu685Giux+ZSWIlDi3zL4NWz+rcvIhO\nKoVAPPYawIAhOszSg3IGD0eVP1I7al5sME7GZPM3kPwxTNCv8z91mPaSyI5/k88c\nm62Fijn4k92kWDOMF7ANFST6JhujB8OitHLFjB1Rapi0H0TpgAtiHCNYcgmFL66e\nlOkmbGzzjdQGIGfY7H3owmkuI5FMycV5TYDkLChasYSlW0sQSJD1j1CiM+qSAE7Y\npWiuEVjrSYgJlcQfBnpQjkjAp2F4bp0TDQT7e+MNCOfA+lUdSWKyZVTLHF0=\n-----END CERTIFICATE-----"
}'
```

###### Response

```json
{
	"id": 9,
	"method": "custom",
	"privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDem6F7HLVxStq/\n/t6aThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0\nzkVT2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCg\nuVkSehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5am\nLU3iSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWro\nCEIQ61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvo\nrn+YPEvDAgMBAAECggEAXOcIqo+dra12w8ADeNRxx8LdW+IMaLOLz6euxbKLUzd9\nlQC0WIKf86jR66B7uljr6HVPsNOXb5rKt/wJ9OdTUW33nn3YKe/41j1VfnbD1pme\nHWFwGP+LpINt7EvzsFEYNDiwV7p+neUqVyljYhGtlBJI70+5dEg4ihBVuWolcQai\nH06XxdT72O41RKr/uSzKcuxZBQhdD9Y505UVMsHOjFu0HMUMZ/wk7msZg4nyYmfg\nW0h/LbzmzblbEzF+wd4PYTE5FC4SRiS1/E8aDe78Rg7ww7pxgW9OvGlMWv4lrfco\nBO430h+VBodQnHHaTj4lV59pYhQGvlHwkSDEHpuWAQKBgQD3Jh6sIY97kOCJ9oET\nlhsBZ6JICiqmsieWRuSmMoiErBezVZ9QS5C5GOL4GkPsc7Bw0s6RB+9TZsEPS3DH\nglpzr5a89d3BRlkSNFSSHeq/W8/s8DYvMoQeapYTr4ijH97y18IjChozx5fcmzQS\nY8J0hYOruabZgyjBvSABNxokwwKBgQDmlIR3Mj7YQsSyyvgOp0r0e1u2zNnnm5n6\nC/kZR1SuhVjQ2tdwPafqjdSoFimUMPrSmJBT6U7LqfEtRaByR9xEHR0d3qb8Ofni\nSngD4NWmPSX6GnWwJVsPHDSqn6JW1eEdAwMisB7F4FhRmvdffmIaVI89HZSuv5gl\n/uEQRGbNAQKBgH+ODQaJy6PaggiyUKvrLMTs17SWiqy+BfBpZljge9T9fL3x0ud+\nGJNvZLTn2WaPzuBr7HCtx7cjsUBTj0Fo5YYPeZzMyEaYKCBdIcjH6AAbQpTm5RA2\n4jlQiWRflAWczVRIRsoOzLcsrBQPhjB3jETXI73dc1+PcdmL4pi996BBAoGAEuob\n86srfJH9kK0VrB4NCAEWhOhI97bL6rcQuAIh8C8AGiHZiluEark3uJIY1w8thBj/\nveJllE9ceVo8zyMV7oB04v5gtFANL4LsVWUcIYbilqGVBd4KmjK2H0j5CCaDUN3u\nY+oOnCzLEeakZDD52y8UkO7cQ3l0AanmzG7QAAECgYBVTTgYqAoUG6wVuQw5Jrnz\n9/ItvU4zPKDadnoIRgMZq+wXo8f8dMSzNjlylv+vQF99E9NRMMIVbyeH4/q5ANGl\nkcFwSyAhE7z+uSV0Ho6OqZVqis8e9rcin9+opC5BePQAjgAyAYciXpE95YXQcYMM\nlXCUsdHQomIAwFN2TNWohQ==\n-----END PRIVATE KEY-----",
	"certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqACCQC6DH7qQzl7LTANBgkqhkiG9w0BAQsFADCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wHhcNMTgwMzAyMDMyNjI3WhcNMTkwMzAyMDMyNjI3WjCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDem6F7HLVxStq//t6a\nThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0zkVT\n2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCguVkS\nehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5amLU3i\nSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWroCEIQ\n61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvorn+Y\nPEvDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA+HxyKESRAzUmDt1aE0v6u95QBB\niGGhVszAviF6ZKaf6bov0Ozq+67Pq+3Tksu685Giux+ZSWIlDi3zL4NWz+rcvIhO\nKoVAPPYawIAhOszSg3IGD0eVP1I7al5sME7GZPM3kPwxTNCv8z91mPaSyI5/k88c\nm62Fijn4k92kWDOMF7ANFST6JhujB8OitHLFjB1Rapi0H0TpgAtiHCNYcgmFL66e\nlOkmbGzzjdQGIGfY7H3owmkuI5FMycV5TYDkLChasYSlW0sQSJD1j1CiM+qSAE7Y\npWiuEVjrSYgJlcQfBnpQjkjAp2F4bp0TDQT7e+MNCOfA+lUdSWKyZVTLHF0=\n-----END CERTIFICATE-----",
	"validUntil": "2019-03-02 03:26:27",
	"renewalDate": null,
	"enableHttp": false,
	"enableHsts": true,
	"authorizationMethod": null,
	"staging": false,
	"api_id": null,
	"ssl_protocol_id": 3,
	"created_at": "2019-06-30 11:37:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Basic - SSL Object

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}/ssl`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/ssl
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/ssl \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 9,
	"method": "custom",
	"privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDem6F7HLVxStq/\n/t6aThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0\nzkVT2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCg\nuVkSehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5am\nLU3iSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWro\nCEIQ61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvo\nrn+YPEvDAgMBAAECggEAXOcIqo+dra12w8ADeNRxx8LdW+IMaLOLz6euxbKLUzd9\nlQC0WIKf86jR66B7uljr6HVPsNOXb5rKt/wJ9OdTUW33nn3YKe/41j1VfnbD1pme\nHWFwGP+LpINt7EvzsFEYNDiwV7p+neUqVyljYhGtlBJI70+5dEg4ihBVuWolcQai\nH06XxdT72O41RKr/uSzKcuxZBQhdD9Y505UVMsHOjFu0HMUMZ/wk7msZg4nyYmfg\nW0h/LbzmzblbEzF+wd4PYTE5FC4SRiS1/E8aDe78Rg7ww7pxgW9OvGlMWv4lrfco\nBO430h+VBodQnHHaTj4lV59pYhQGvlHwkSDEHpuWAQKBgQD3Jh6sIY97kOCJ9oET\nlhsBZ6JICiqmsieWRuSmMoiErBezVZ9QS5C5GOL4GkPsc7Bw0s6RB+9TZsEPS3DH\nglpzr5a89d3BRlkSNFSSHeq/W8/s8DYvMoQeapYTr4ijH97y18IjChozx5fcmzQS\nY8J0hYOruabZgyjBvSABNxokwwKBgQDmlIR3Mj7YQsSyyvgOp0r0e1u2zNnnm5n6\nC/kZR1SuhVjQ2tdwPafqjdSoFimUMPrSmJBT6U7LqfEtRaByR9xEHR0d3qb8Ofni\nSngD4NWmPSX6GnWwJVsPHDSqn6JW1eEdAwMisB7F4FhRmvdffmIaVI89HZSuv5gl\n/uEQRGbNAQKBgH+ODQaJy6PaggiyUKvrLMTs17SWiqy+BfBpZljge9T9fL3x0ud+\nGJNvZLTn2WaPzuBr7HCtx7cjsUBTj0Fo5YYPeZzMyEaYKCBdIcjH6AAbQpTm5RA2\n4jlQiWRflAWczVRIRsoOzLcsrBQPhjB3jETXI73dc1+PcdmL4pi996BBAoGAEuob\n86srfJH9kK0VrB4NCAEWhOhI97bL6rcQuAIh8C8AGiHZiluEark3uJIY1w8thBj/\nveJllE9ceVo8zyMV7oB04v5gtFANL4LsVWUcIYbilqGVBd4KmjK2H0j5CCaDUN3u\nY+oOnCzLEeakZDD52y8UkO7cQ3l0AanmzG7QAAECgYBVTTgYqAoUG6wVuQw5Jrnz\n9/ItvU4zPKDadnoIRgMZq+wXo8f8dMSzNjlylv+vQF99E9NRMMIVbyeH4/q5ANGl\nkcFwSyAhE7z+uSV0Ho6OqZVqis8e9rcin9+opC5BePQAjgAyAYciXpE95YXQcYMM\nlXCUsdHQomIAwFN2TNWohQ==\n-----END PRIVATE KEY-----",
	"certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqACCQC6DH7qQzl7LTANBgkqhkiG9w0BAQsFADCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wHhcNMTgwMzAyMDMyNjI3WhcNMTkwMzAyMDMyNjI3WjCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDem6F7HLVxStq//t6a\nThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0zkVT\n2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCguVkS\nehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5amLU3i\nSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWroCEIQ\n61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvorn+Y\nPEvDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA+HxyKESRAzUmDt1aE0v6u95QBB\niGGhVszAviF6ZKaf6bov0Ozq+67Pq+3Tksu685Giux+ZSWIlDi3zL4NWz+rcvIhO\nKoVAPPYawIAhOszSg3IGD0eVP1I7al5sME7GZPM3kPwxTNCv8z91mPaSyI5/k88c\nm62Fijn4k92kWDOMF7ANFST6JhujB8OitHLFjB1Rapi0H0TpgAtiHCNYcgmFL66e\nlOkmbGzzjdQGIGfY7H3owmkuI5FMycV5TYDkLChasYSlW0sQSJD1j1CiM+qSAE7Y\npWiuEVjrSYgJlcQfBnpQjkjAp2F4bp0TDQT7e+MNCOfA+lUdSWKyZVTLHF0=\n-----END CERTIFICATE-----",
	"validUntil": "2019-03-02 03:26:27",
	"renewalDate": null,
	"enableHttp": false,
	"enableHsts": true,
	"authorizationMethod": null,
	"staging": false,
	"api_id": null,
	"ssl_protocol_id": 3,
	"created_at": "2019-06-30 11:37:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Basic - Update SSL Config

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>enableHttp</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean true or false</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>enableHsts</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean true or false</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>ssl_protocol_id</span>
                <span class="text-danger">OPTIONAL</span>
            </td>
            <td>Integer</td>
            <td>SSL Protocol. You can get this value from Static Data API. It will current value if not supplied.</td>
        </tr>
        <tr>
            <td>
                <span>privateKey</span>
                <span>required if provider is "custom"</span>
            </td>
            <td>SSL Private Key</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>certificate</span>
                <span>required if provider is "custom"</span>
            </td>
            <td>SSL Certificate</td>
            <td></td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/webapps/{webAppId}/ssl/{sslId}`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/ssl/{sslId}
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/ssl/9 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "enableHttp": true,
  "enableHsts": false,
  "ssl_protocol_id": 2,
  "privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDem6F7HLVxStq/\n/t6aThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0\nzkVT2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCg\nuVkSehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5am\nLU3iSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWro\nCEIQ61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvo\nrn+YPEvDAgMBAAECggEAXOcIqo+dra12w8ADeNRxx8LdW+IMaLOLz6euxbKLUzd9\nlQC0WIKf86jR66B7uljr6HVPsNOXb5rKt/wJ9OdTUW33nn3YKe/41j1VfnbD1pme\nHWFwGP+LpINt7EvzsFEYNDiwV7p+neUqVyljYhGtlBJI70+5dEg4ihBVuWolcQai\nH06XxdT72O41RKr/uSzKcuxZBQhdD9Y505UVMsHOjFu0HMUMZ/wk7msZg4nyYmfg\nW0h/LbzmzblbEzF+wd4PYTE5FC4SRiS1/E8aDe78Rg7ww7pxgW9OvGlMWv4lrfco\nBO430h+VBodQnHHaTj4lV59pYhQGvlHwkSDEHpuWAQKBgQD3Jh6sIY97kOCJ9oET\nlhsBZ6JICiqmsieWRuSmMoiErBezVZ9QS5C5GOL4GkPsc7Bw0s6RB+9TZsEPS3DH\nglpzr5a89d3BRlkSNFSSHeq/W8/s8DYvMoQeapYTr4ijH97y18IjChozx5fcmzQS\nY8J0hYOruabZgyjBvSABNxokwwKBgQDmlIR3Mj7YQsSyyvgOp0r0e1u2zNnnm5n6\nC/kZR1SuhVjQ2tdwPafqjdSoFimUMPrSmJBT6U7LqfEtRaByR9xEHR0d3qb8Ofni\nSngD4NWmPSX6GnWwJVsPHDSqn6JW1eEdAwMisB7F4FhRmvdffmIaVI89HZSuv5gl\n/uEQRGbNAQKBgH+ODQaJy6PaggiyUKvrLMTs17SWiqy+BfBpZljge9T9fL3x0ud+\nGJNvZLTn2WaPzuBr7HCtx7cjsUBTj0Fo5YYPeZzMyEaYKCBdIcjH6AAbQpTm5RA2\n4jlQiWRflAWczVRIRsoOzLcsrBQPhjB3jETXI73dc1+PcdmL4pi996BBAoGAEuob\n86srfJH9kK0VrB4NCAEWhOhI97bL6rcQuAIh8C8AGiHZiluEark3uJIY1w8thBj/\nveJllE9ceVo8zyMV7oB04v5gtFANL4LsVWUcIYbilqGVBd4KmjK2H0j5CCaDUN3u\nY+oOnCzLEeakZDD52y8UkO7cQ3l0AanmzG7QAAECgYBVTTgYqAoUG6wVuQw5Jrnz\n9/ItvU4zPKDadnoIRgMZq+wXo8f8dMSzNjlylv+vQF99E9NRMMIVbyeH4/q5ANGl\nkcFwSyAhE7z+uSV0Ho6OqZVqis8e9rcin9+opC5BePQAjgAyAYciXpE95YXQcYMM\nlXCUsdHQomIAwFN2TNWohQ==\n-----END PRIVATE KEY-----",
    "certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqACCQC6DH7qQzl7LTANBgkqhkiG9w0BAQsFADCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wHhcNMTgwMzAyMDMyNjI3WhcNMTkwMzAyMDMyNjI3WjCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDem6F7HLVxStq//t6a\nThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0zkVT\n2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCguVkS\nehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5amLU3i\nSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWroCEIQ\n61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvorn+Y\nPEvDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA+HxyKESRAzUmDt1aE0v6u95QBB\niGGhVszAviF6ZKaf6bov0Ozq+67Pq+3Tksu685Giux+ZSWIlDi3zL4NWz+rcvIhO\nKoVAPPYawIAhOszSg3IGD0eVP1I7al5sME7GZPM3kPwxTNCv8z91mPaSyI5/k88c\nm62Fijn4k92kWDOMF7ANFST6JhujB8OitHLFjB1Rapi0H0TpgAtiHCNYcgmFL66e\nlOkmbGzzjdQGIGfY7H3owmkuI5FMycV5TYDkLChasYSlW0sQSJD1j1CiM+qSAE7Y\npWiuEVjrSYgJlcQfBnpQjkjAp2F4bp0TDQT7e+MNCOfA+lUdSWKyZVTLHF0=\n-----END CERTIFICATE-----"
}'
```

###### Response

```json
{
	"id": 9,
	"method": "custom",
	"privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDem6F7HLVxStq/\n/t6aThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0\nzkVT2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCg\nuVkSehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5am\nLU3iSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWro\nCEIQ61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvo\nrn+YPEvDAgMBAAECggEAXOcIqo+dra12w8ADeNRxx8LdW+IMaLOLz6euxbKLUzd9\nlQC0WIKf86jR66B7uljr6HVPsNOXb5rKt/wJ9OdTUW33nn3YKe/41j1VfnbD1pme\nHWFwGP+LpINt7EvzsFEYNDiwV7p+neUqVyljYhGtlBJI70+5dEg4ihBVuWolcQai\nH06XxdT72O41RKr/uSzKcuxZBQhdD9Y505UVMsHOjFu0HMUMZ/wk7msZg4nyYmfg\nW0h/LbzmzblbEzF+wd4PYTE5FC4SRiS1/E8aDe78Rg7ww7pxgW9OvGlMWv4lrfco\nBO430h+VBodQnHHaTj4lV59pYhQGvlHwkSDEHpuWAQKBgQD3Jh6sIY97kOCJ9oET\nlhsBZ6JICiqmsieWRuSmMoiErBezVZ9QS5C5GOL4GkPsc7Bw0s6RB+9TZsEPS3DH\nglpzr5a89d3BRlkSNFSSHeq/W8/s8DYvMoQeapYTr4ijH97y18IjChozx5fcmzQS\nY8J0hYOruabZgyjBvSABNxokwwKBgQDmlIR3Mj7YQsSyyvgOp0r0e1u2zNnnm5n6\nC/kZR1SuhVjQ2tdwPafqjdSoFimUMPrSmJBT6U7LqfEtRaByR9xEHR0d3qb8Ofni\nSngD4NWmPSX6GnWwJVsPHDSqn6JW1eEdAwMisB7F4FhRmvdffmIaVI89HZSuv5gl\n/uEQRGbNAQKBgH+ODQaJy6PaggiyUKvrLMTs17SWiqy+BfBpZljge9T9fL3x0ud+\nGJNvZLTn2WaPzuBr7HCtx7cjsUBTj0Fo5YYPeZzMyEaYKCBdIcjH6AAbQpTm5RA2\n4jlQiWRflAWczVRIRsoOzLcsrBQPhjB3jETXI73dc1+PcdmL4pi996BBAoGAEuob\n86srfJH9kK0VrB4NCAEWhOhI97bL6rcQuAIh8C8AGiHZiluEark3uJIY1w8thBj/\nveJllE9ceVo8zyMV7oB04v5gtFANL4LsVWUcIYbilqGVBd4KmjK2H0j5CCaDUN3u\nY+oOnCzLEeakZDD52y8UkO7cQ3l0AanmzG7QAAECgYBVTTgYqAoUG6wVuQw5Jrnz\n9/ItvU4zPKDadnoIRgMZq+wXo8f8dMSzNjlylv+vQF99E9NRMMIVbyeH4/q5ANGl\nkcFwSyAhE7z+uSV0Ho6OqZVqis8e9rcin9+opC5BePQAjgAyAYciXpE95YXQcYMM\nlXCUsdHQomIAwFN2TNWohQ==\n-----END PRIVATE KEY-----",
	"certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqACCQC6DH7qQzl7LTANBgkqhkiG9w0BAQsFADCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wHhcNMTgwMzAyMDMyNjI3WhcNMTkwMzAyMDMyNjI3WjCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDem6F7HLVxStq//t6a\nThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0zkVT\n2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCguVkS\nehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5amLU3i\nSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWroCEIQ\n61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvorn+Y\nPEvDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA+HxyKESRAzUmDt1aE0v6u95QBB\niGGhVszAviF6ZKaf6bov0Ozq+67Pq+3Tksu685Giux+ZSWIlDi3zL4NWz+rcvIhO\nKoVAPPYawIAhOszSg3IGD0eVP1I7al5sME7GZPM3kPwxTNCv8z91mPaSyI5/k88c\nm62Fijn4k92kWDOMF7ANFST6JhujB8OitHLFjB1Rapi0H0TpgAtiHCNYcgmFL66e\nlOkmbGzzjdQGIGfY7H3owmkuI5FMycV5TYDkLChasYSlW0sQSJD1j1CiM+qSAE7Y\npWiuEVjrSYgJlcQfBnpQjkjAp2F4bp0TDQT7e+MNCOfA+lUdSWKyZVTLHF0=\n-----END CERTIFICATE-----",
	"validUntil": "2019-03-02 03:26:27",
	"renewalDate": null,
	"enableHttp": true,
	"enableHsts": false,
	"authorizationMethod": null,
	"staging": false,
	"api_id": null,
	"ssl_protocol_id": 2,
	"created_at": "2019-06-30 11:37:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Basic - Redeploy SSL (Let's Encrypt only)

</div>
<div class="col col-xl-6 dark">

`PUT /servers/{serverId}/webapps/{webAppId}/ssl/{sslId}`

###### HTTP Request

```
PUT https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/ssl/{sslId}
```

###### Request

```bash
curl --request PUT \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/ssl/10 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 10,
	"method": "letsencrypt",
	"privateKey": null,
	"certificate": null,
	"validUntil": null,
	"renewalDate": null,
	"enableHttp": true,
	"enableHsts": false,
	"authorizationMethod": "dns-01",
	"staging": true,
	"api_id": 2,
	"ssl_protocol_id": 2,
	"created_at": "2019-06-30 13:43:38"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Basic - Delete SSL

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/webapps/{webAppId}/ssl/{sslId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/ssl/{sslId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/ssl/9 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 9,
	"method": "custom",
	"privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDem6F7HLVxStq/\n/t6aThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0\nzkVT2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCg\nuVkSehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5am\nLU3iSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWro\nCEIQ61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvo\nrn+YPEvDAgMBAAECggEAXOcIqo+dra12w8ADeNRxx8LdW+IMaLOLz6euxbKLUzd9\nlQC0WIKf86jR66B7uljr6HVPsNOXb5rKt/wJ9OdTUW33nn3YKe/41j1VfnbD1pme\nHWFwGP+LpINt7EvzsFEYNDiwV7p+neUqVyljYhGtlBJI70+5dEg4ihBVuWolcQai\nH06XxdT72O41RKr/uSzKcuxZBQhdD9Y505UVMsHOjFu0HMUMZ/wk7msZg4nyYmfg\nW0h/LbzmzblbEzF+wd4PYTE5FC4SRiS1/E8aDe78Rg7ww7pxgW9OvGlMWv4lrfco\nBO430h+VBodQnHHaTj4lV59pYhQGvlHwkSDEHpuWAQKBgQD3Jh6sIY97kOCJ9oET\nlhsBZ6JICiqmsieWRuSmMoiErBezVZ9QS5C5GOL4GkPsc7Bw0s6RB+9TZsEPS3DH\nglpzr5a89d3BRlkSNFSSHeq/W8/s8DYvMoQeapYTr4ijH97y18IjChozx5fcmzQS\nY8J0hYOruabZgyjBvSABNxokwwKBgQDmlIR3Mj7YQsSyyvgOp0r0e1u2zNnnm5n6\nC/kZR1SuhVjQ2tdwPafqjdSoFimUMPrSmJBT6U7LqfEtRaByR9xEHR0d3qb8Ofni\nSngD4NWmPSX6GnWwJVsPHDSqn6JW1eEdAwMisB7F4FhRmvdffmIaVI89HZSuv5gl\n/uEQRGbNAQKBgH+ODQaJy6PaggiyUKvrLMTs17SWiqy+BfBpZljge9T9fL3x0ud+\nGJNvZLTn2WaPzuBr7HCtx7cjsUBTj0Fo5YYPeZzMyEaYKCBdIcjH6AAbQpTm5RA2\n4jlQiWRflAWczVRIRsoOzLcsrBQPhjB3jETXI73dc1+PcdmL4pi996BBAoGAEuob\n86srfJH9kK0VrB4NCAEWhOhI97bL6rcQuAIh8C8AGiHZiluEark3uJIY1w8thBj/\nveJllE9ceVo8zyMV7oB04v5gtFANL4LsVWUcIYbilqGVBd4KmjK2H0j5CCaDUN3u\nY+oOnCzLEeakZDD52y8UkO7cQ3l0AanmzG7QAAECgYBVTTgYqAoUG6wVuQw5Jrnz\n9/ItvU4zPKDadnoIRgMZq+wXo8f8dMSzNjlylv+vQF99E9NRMMIVbyeH4/q5ANGl\nkcFwSyAhE7z+uSV0Ho6OqZVqis8e9rcin9+opC5BePQAjgAyAYciXpE95YXQcYMM\nlXCUsdHQomIAwFN2TNWohQ==\n-----END PRIVATE KEY-----",
	"certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqACCQC6DH7qQzl7LTANBgkqhkiG9w0BAQsFADCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wHhcNMTgwMzAyMDMyNjI3WhcNMTkwMzAyMDMyNjI3WjCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDem6F7HLVxStq//t6a\nThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0zkVT\n2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCguVkS\nehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5amLU3i\nSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWroCEIQ\n61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvorn+Y\nPEvDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA+HxyKESRAzUmDt1aE0v6u95QBB\niGGhVszAviF6ZKaf6bov0Ozq+67Pq+3Tksu685Giux+ZSWIlDi3zL4NWz+rcvIhO\nKoVAPPYawIAhOszSg3IGD0eVP1I7al5sME7GZPM3kPwxTNCv8z91mPaSyI5/k88c\nm62Fijn4k92kWDOMF7ANFST6JhujB8OitHLFjB1Rapi0H0TpgAtiHCNYcgmFL66e\nlOkmbGzzjdQGIGfY7H3owmkuI5FMycV5TYDkLChasYSlW0sQSJD1j1CiM+qSAE7Y\npWiuEVjrSYgJlcQfBnpQjkjAp2F4bp0TDQT7e+MNCOfA+lUdSWKyZVTLHF0=\n-----END CERTIFICATE-----",
	"validUntil": "2019-03-02 03:26:27",
	"renewalDate": null,
	"enableHttp": true,
	"enableHsts": false,
	"authorizationMethod": null,
	"staging": false,
	"api_id": null,
	"ssl_protocol_id": 2,
	"created_at": "2019-06-30 11:37:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Advanced - Status

###### <span class="text-danger">Note : This API endpoint is available for Business plan user only. For non Business user, 403 HTTP response is returned.</span>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}/ssl/advanced`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/ssl/advanced
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/ssl/advanced \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"advancedSSL": false,
	"autoSSL": false
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Advanced - Switching SSL

###### <span class="text-danger">Note : This API endpoint is available for Business plan user only. For non Business user, 403 HTTP response is returned.</span>

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>advancedSSL</span>
                <span class="text-danger">required</span>
            </td>
            <td>boolean</td>
            <td>Enable advanced SSL</td>
        </tr>
        <tr>
            <td>
                <span>autoSSL</span>
                <span class="text-danger">optional</span>
            </td>
            <td>boolean</td>
            <td>Auto SSL will automatically set to "false" by default</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/webapps/{webAppId}/ssl/advanced`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/ssl/advanced
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/ssl/advanced \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
        "advancedSSL" : true,
        "autoSSL"     : false
    }'
```

###### Response

```json
{
	"advancedSSL": true,
	"autoSSL": false
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Advanced - Install SSL

###### <span class="text-danger">Note : This API endpoint is available for Business plan user only. For non Business user, 403 HTTP response is returned.</span>

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>provider</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"letsencrypt" , "custom" or "csr"</td>
        </tr>
        <tr>
            <td>
                <span>enableHttp</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Either "true" or "false"</td>
        </tr>
        <tr>
            <td>
                <span>enableHsts</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td>Either "true" or "false"</td>
        </tr>
        <tr>
            <td>
                <span>ssl_protocol_id</span>
                <span class="text-danger">Optional</span>
            </td>
            <td>Integer</td>
            <td>SSL Protocol. You can get this value from Static Data API. It will use latest possible value if not supplied.</td>
        </tr>
        <tr>
            <td>
                <span>authorizationMethod</span>
                <span>required if provider is "letsencrypt"</span>
            </td>
            <td>String</td>
            <td>"http-01" or "dns-01"</td>
        </tr>
        <tr>
            <td>
                <span>externalApi</span>
                <span>required if provider is "letsencrypt" and authorizationMethod is "dns-01"</span>
            </td>
            <td>Integer</td>
            <td>Id of the 3rd Party API to use. </td>
        </tr>
        <tr>
            <td>
                <span>environment</span>
                <span>required if provider is "letsencrypt"</span>
            </td>
            <td>String</td>
            <td>"live" or "staging" environment</td>
        </tr>
        <tr>
            <td>
                <span>privateKey</span>
                <span>required if provider is "custom"</span>
            </td>
            <td>RSA Private Key String</td>
            <td>SSL Private Key</td>
        </tr>
        <tr>
            <td>
                <span>certificate</span>
                <span>required if provider is "custom"</span>
            </td>
            <td>RSA Public Key String</td>
            <td>SSL Certificate</td>
        </tr>
        <tr>
            <td>
                <span>csrKeyType</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>"rsa-2048", "ecdsa-p384", "ecdsa-p256" and "rsa-4096"</td>
        </tr>
        <tr>
            <td>
                <span>organization</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your organization name, default value was RunCloud Organization</td>
        </tr>
        <tr>
            <td>
                <span>department</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your department name, default value was RunCloud Department</td>
        </tr>
        <tr>
            <td>
                <span>city</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your city name, default value for city where RunCloud Organization has been found</td>
        </tr>
        <tr>
            <td>
                <span>state</span>
               <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your state name, default value for state where RunCloud Organization has been found</td>
        </tr>
        <tr>
            <td>
                <span>country</span>
                <span>Optional if provider is csr</span>
            </td>
            <td>String</td>
            <td>Your country name, default value for country where RunCloud Organization has been found</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`POST /servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl`

###### HTTP Request

```
POST https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl
```

###### Request

```bash
curl --request POST \
  --url https://{{ API_BASE_PATH }}/servers/9/webapps/52/domains/2/ssl \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
    "provider": "csr",
    "enableHttp": false,
    "enableHsts": true
}'
```

###### Response

```json
{
	"id": 1,
	"method": "csr",
	"privateKey": "-----BEGIN RSA PRIVATE KEY-----\r\nMIIEpQIBAAKCAQEA1LAZqcYdFSWppXG6UvUSD0+44GZ5ThOkMi0cb6FxI/m4tjbk\r\nd3AHvWzk1qeZhfMjpuhqrhjYQOG4DwACeACsBZCpxX5VFfi06ubHXTS0xHNLoi7L\r\neR1S5td8yWUkhBZwUpTQ9wUWLC66TpNauRvn/R6PwwGQHIJk9cNDQHoyDQVd8MP4\r\n4XV6+F5Dwp8eQiv8fDMbhBbO9IJguo2tA3jG9cxtpGL84mt+QKKGq8WGPalADWT0\r\nQ7DcOMVrK4FoytaiPJ5tjFqxmv6V/n3QCzRYyXQMM2dOOycveg8ImuXr3SlgMP4l\r\n8ZrsM/I8dn3/5uKvCvBWEhD1vFTsgjRLftooTwIDAQABAoIBAQCtUaw4G4fLmdt7\r\nRQhPXgKPk6nWw+sKYxslAHnxxh/K709EvcrEQfkstjZtbjq/W3msJC37Hafsh4RU\r\nRO++Ft4X0uFmXSK9OuVDnzjLjY3ZoRL7YSLS2cCa+PNpsxBky1xRD5CSHAULkDCm\r\no5IE0ubj8Cm7AeUMNn5LgAmn22w6Cq7xax9tmX9hnWggtRorViwvuxrVZFeYA/iO\r\n1ycp/e0nf23CQDwNDdZnggMHHOjc3rTBc/pJja9iFE45y254l7nOJCNBFkiIItQV\r\nOzz8fqu56FQqc4GmxRV4hIQVNZBgz447cXIlYOno97Wqm+rB3UDx7GXV8csVk108\r\njF5nXDqRAoGBAPOQ2mLrR+r4fHdEk4NHTCcWhBVYJfvhgqgyPq6oE8KTIAlWDa1d\r\nsJDF3rPKZQiOXNLz0ja+xSmQMo4O1/uZGrmQSeb0fxQjGWqhgHd7HcD76hkEDckk\r\nncoTL2rWSjdRngEJLvv0U2tvHJfoOBYqirvOSwrc67UiZ8zfCxwCY7wtAoGBAN+L\r\ntIv6aTUtWV5KO0vT/2fZBJgrMmduLDrwju3JCQF7vY5WL1q/iXijGF5OsnMmV3P0\r\nK8mU8z92hZMjf7tO+5pDIgs4/N1L0Uj3qLE26/pdCS/7SNt3C31cjGYWoEyj23JG\r\n0xPUe1fN8acdKxxpzz8BpCdyWa2qUVnjVAF60ffrAoGBAOG5HKsP4VahcrgVlZ/U\r\nM38GHuKpDoqgM7Ys2mqC2c05DDeayJIigb+poPp+V6PWS7A0xlx178LVIAUHPKKv\r\nAVV3owWnER5YaPWpNqWEM87lp84HM791b8YWgZtBfQZHXWcYW7Kke6rVDmq5i0D/\r\nrBLg3Cr1EnyLpib3jhQnIscBAoGAeeQ8i0L8kPFsVHyXfo5XKrHrzmlnb3xkg7Ld\r\nzIyc64EkZ/NYmNvtwqjzxaCVrzIN530RTzZ3uHgQ950YrfKwQHowmvv07VnFAUuB\r\nYTf4TijitJYJMRnlWvLW32eg+okyREqYF6z5oHP8O0otUq3jQpDLiHLSsV44Q5Iw\r\nISpuSQcCgYEAzPyzlWEk/PNdlu+kKQyRhrPeCAp4rdCrqYPD40Lf9KVftTeyUZru\r\nhLX5aGIIgm5Ke1nk+gDmmgMb98RxO6LAfPEreMqujqtAIEU5htksMrmDIi4aOCLR\r\ni3/Y3EtnFt5B380TinFOWTZcy+zuBbAsH0V8RPHT3NoxXh6fmUZnI+I=\r\n-----END RSA PRIVATE KEY-----",
	"certificate": "-----BEGIN CERTIFICATE-----\r\nMIIEMTCCAxmgAwIBAgIUQmYhdrAOzmtTBMJaa65q5PlEemIwDQYJKoZIhvcNAQEL\r\nBQAwgakxCzAJBgNVBAYTAk1ZMREwDwYDVQQIDAhTZWxhbmdvcjESMBAGA1UEBwwJ\r\nQ3liZXJqYXlhMRswGQYDVQQKDBJSdW5DbG91ZCBTZG4uIEJoZC4xFDASBgNVBAsM\r\nC0VuZ2luZWVyaW5nMSAwHgYDVQQDDBdSdW5DbG91ZCBXZWJTU0wgUm9vdCBDQTEe\r\nMBwGCSqGSIb3DQEJARYPZGV2QHJ1bmNsb3VkLmlvMB4XDTIxMDcwNDAyMDM0MFoX\r\nDTIzMTAwNDAyMDM0MFowcTETMBEGA1UECgwKTXkgQ29tcGFueTELMAkGA1UECwwC\r\nSVQxDTALBgNVBAcMBENpdHkxDjAMBgNVBAgMBVN0YXRlMQswCQYDVQQGDAJVUzEh\r\nMB8GA1UEAwwYUnVuQ2xvdWQgV2ViIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0B\r\nAQEFAAOCAQ8AMIIBCgKCAQEA1LAZqcYdFSWppXG6UvUSD0+44GZ5ThOkMi0cb6Fx\r\nI/m4tjbkd3AHvWzk1qeZhfMjpuhqrhjYQOG4DwACeACsBZCpxX5VFfi06ubHXTS0\r\nxHNLoi7LeR1S5td8yWUkhBZwUpTQ9wUWLC66TpNauRvn/R6PwwGQHIJk9cNDQHoy\r\nDQVd8MP44XV6+F5Dwp8eQiv8fDMbhBbO9IJguo2tA3jG9cxtpGL84mt+QKKGq8WG\r\nPalADWT0Q7DcOMVrK4FoytaiPJ5tjFqxmv6V/n3QCzRYyXQMM2dOOycveg8ImuXr\r\n3SlgMP4l8ZrsM/I8dn3/5uKvCvBWEhD1vFTsgjRLftooTwIDAQABo4GHMIGEMA4G\r\nA1UdDwEB/wQEAwIF4DAPBgNVHRMBAf8EBTADAgEAMB0GA1UdJQQWMBQGCCsGAQUF\r\nBwMBBggrBgEFBQcDAjAhBgNVHREEGjAYgglrdWNpbmcucmOCCyoua3VjaW5nLnJj\r\nMB8GA1UdIwQYMBaAFLLyby7gHA8E+5L84arEyafndfixMA0GCSqGSIb3DQEBCwUA\r\nA4IBAQBnMSKMz7DNm03OVrhPydxS6X6n2Ma6IucyWwrggPlQf0DjqZe8DpTZq+Zm\r\nRZmPvx0f4oL1JnWpRkG+BUTUlQtM0Db70ezrRqWGzll9tCQhkagrIGQGi51TeYf9\r\nY8lhTOwSIBP9c17jCuAxrAajZVcHW+jJTSc+ExFYBhIuiWQqH8PAFNfxMXRqZ6Lp\r\nDBz16iUqGRnweuus51jK1gZkr356k2RIQFErhXHQobuC48+bt0fWVl+VaDfaXpnT\r\nngG7TOtzGDnzwcOgNHHFMaRkb4PFMImx7ogHt5/7NtlWJh80YknO35q7Ba+weHpH\r\nzD+o3gRLl7EoQB/Wi48M7zCoRb5D\r\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIERDCCAyygAwIBAgIUZ8MfP1GxDkapNKf5PSBNF/KNifcwDQYJKoZIhvcNAQEL\nBQAwgakxCzAJBgNVBAYTAk1ZMREwDwYDVQQIDAhTZWxhbmdvcjESMBAGA1UEBwwJ\nQ3liZXJqYXlhMRswGQYDVQQKDBJSdW5DbG91ZCBTZG4uIEJoZC4xFDASBgNVBAsM\nC0VuZ2luZWVyaW5nMSAwHgYDVQQDDBdSdW5DbG91ZCBXZWJTU0wgUm9vdCBDQTEe\nMBwGCSqGSIb3DQEJARYPZGV2QHJ1bmNsb3VkLmlvMCAXDTIxMDQyMTA2MDgwN1oY\nDzIxMjEwMzI4MDYwODA3WjCBqTELMAkGA1UEBhMCTVkxETAPBgNVBAgMCFNlbGFu\nZ29yMRIwEAYDVQQHDAlDeWJlcmpheWExGzAZBgNVBAoMElJ1bkNsb3VkIFNkbi4g\nQmhkLjEUMBIGA1UECwwLRW5naW5lZXJpbmcxIDAeBgNVBAMMF1J1bkNsb3VkIFdl\nYlNTTCBSb290IENBMR4wHAYJKoZIhvcNAQkBFg9kZXZAcnVuY2xvdWQuaW8wggEi\nMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDs1I3diSCO/yrC8n2zkK8QVqup\nhWt8fYH9AcqfuvB0OV4/a3U6ojMvm9fRF3Vw0woI3lfFoklNDspe3SK9O01yBaGm\nCDU1FMEH6qg2X9LVT5ol9MYgHQRHzj/rWX3CVhzm3sRP7tUKRWVVE+8EZFl0I5bk\nCpzdRc619iSOneTb5Xb9db0A7EcvagvPOrqQzvRZ/RuLyHbNBzfZ90IXkobjTmbq\nihMYDwtFODkArI8k8iG7AXoE12E+j620TzV2rbi7FqZRX9elF8zF5jOijytG4fdr\nDrbXAlGArt3o3P8R99+GXBLhBwHW3PBKxHcU4kzZ4zmMM0+MpajbQwyAg0xzAgMB\nAAGjYDBeMB0GA1UdDgQWBBSy8m8u4BwPBPuS/OGqxMmn53X4sTAfBgNVHSMEGDAW\ngBSy8m8u4BwPBPuS/OGqxMmn53X4sTAMBgNVHRMEBTADAQH/MA4GA1UdDwEB/wQE\nAwIBhjANBgkqhkiG9w0BAQsFAAOCAQEAkDZi83HNJZZDmA3gqX/s2iZF6ooc608g\nBxTRIcPKgLn7aCIlosXOCtmj4rSeLJPARTs1oNUKTdME107MFINDEwjZFkxqb61X\n8kae9q64gQdn4a3k39f+Wjwc9pnklTvaHG0HJbW3zcQhtPU76dW2pstBEJc08jKD\nGqQkmrQSLMQ0RlL1jHnnKBZ6jQtbkS/6QGp3lmVhVvjhlKSnMXChqx4mEliICXi+\nfKozw+SfuwkTyyApJXzjirgy49s+8HE1vlz2ZPG+pg+p6yF/jzkWNjptalbtJjYY\nrPFsJch3y+PidsWl1tP1MCp7IVvs4+o0joIejRRYPlcVQxv9eUP52g==\n-----END CERTIFICATE-----\n",
	"validUntil": "2023-10-04 02:03:40",
	"renewalDate": null,
	"enableHttp": false,
	"enableHsts": true,
	"authorizationMethod": null,
	"staging": false,
	"api_id": null,
	"ssl_protocol_id": 2,
	"created_at": "2021-07-06 02:03:40"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Advanced - SSL Object

###### <span class="text-danger">Note : This API endpoint is available for Business plan user only. For non Business user, 403 HTTP response is returned.</span>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/9/webapps/52/domains/2/ssl \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 1,
	"method": "csr",
	"privateKey": "-----BEGIN RSA PRIVATE KEY-----\r\nMIIEpQIBAAKCAQEA1LAZqcYdFSWppXG6UvUSD0+44GZ5ThOkMi0cb6FxI/m4tjbk\r\nd3AHvWzk1qeZhfMjpuhqrhjYQOG4DwACeACsBZCpxX5VFfi06ubHXTS0xHNLoi7L\r\neR1S5td8yWUkhBZwUpTQ9wUWLC66TpNauRvn/R6PwwGQHIJk9cNDQHoyDQVd8MP4\r\n4XV6+F5Dwp8eQiv8fDMbhBbO9IJguo2tA3jG9cxtpGL84mt+QKKGq8WGPalADWT0\r\nQ7DcOMVrK4FoytaiPJ5tjFqxmv6V/n3QCzRYyXQMM2dOOycveg8ImuXr3SlgMP4l\r\n8ZrsM/I8dn3/5uKvCvBWEhD1vFTsgjRLftooTwIDAQABAoIBAQCtUaw4G4fLmdt7\r\nRQhPXgKPk6nWw+sKYxslAHnxxh/K709EvcrEQfkstjZtbjq/W3msJC37Hafsh4RU\r\nRO++Ft4X0uFmXSK9OuVDnzjLjY3ZoRL7YSLS2cCa+PNpsxBky1xRD5CSHAULkDCm\r\no5IE0ubj8Cm7AeUMNn5LgAmn22w6Cq7xax9tmX9hnWggtRorViwvuxrVZFeYA/iO\r\n1ycp/e0nf23CQDwNDdZnggMHHOjc3rTBc/pJja9iFE45y254l7nOJCNBFkiIItQV\r\nOzz8fqu56FQqc4GmxRV4hIQVNZBgz447cXIlYOno97Wqm+rB3UDx7GXV8csVk108\r\njF5nXDqRAoGBAPOQ2mLrR+r4fHdEk4NHTCcWhBVYJfvhgqgyPq6oE8KTIAlWDa1d\r\nsJDF3rPKZQiOXNLz0ja+xSmQMo4O1/uZGrmQSeb0fxQjGWqhgHd7HcD76hkEDckk\r\nncoTL2rWSjdRngEJLvv0U2tvHJfoOBYqirvOSwrc67UiZ8zfCxwCY7wtAoGBAN+L\r\ntIv6aTUtWV5KO0vT/2fZBJgrMmduLDrwju3JCQF7vY5WL1q/iXijGF5OsnMmV3P0\r\nK8mU8z92hZMjf7tO+5pDIgs4/N1L0Uj3qLE26/pdCS/7SNt3C31cjGYWoEyj23JG\r\n0xPUe1fN8acdKxxpzz8BpCdyWa2qUVnjVAF60ffrAoGBAOG5HKsP4VahcrgVlZ/U\r\nM38GHuKpDoqgM7Ys2mqC2c05DDeayJIigb+poPp+V6PWS7A0xlx178LVIAUHPKKv\r\nAVV3owWnER5YaPWpNqWEM87lp84HM791b8YWgZtBfQZHXWcYW7Kke6rVDmq5i0D/\r\nrBLg3Cr1EnyLpib3jhQnIscBAoGAeeQ8i0L8kPFsVHyXfo5XKrHrzmlnb3xkg7Ld\r\nzIyc64EkZ/NYmNvtwqjzxaCVrzIN530RTzZ3uHgQ950YrfKwQHowmvv07VnFAUuB\r\nYTf4TijitJYJMRnlWvLW32eg+okyREqYF6z5oHP8O0otUq3jQpDLiHLSsV44Q5Iw\r\nISpuSQcCgYEAzPyzlWEk/PNdlu+kKQyRhrPeCAp4rdCrqYPD40Lf9KVftTeyUZru\r\nhLX5aGIIgm5Ke1nk+gDmmgMb98RxO6LAfPEreMqujqtAIEU5htksMrmDIi4aOCLR\r\ni3/Y3EtnFt5B380TinFOWTZcy+zuBbAsH0V8RPHT3NoxXh6fmUZnI+I=\r\n-----END RSA PRIVATE KEY-----",
	"certificate": "-----BEGIN CERTIFICATE-----\r\nMIIEMTCCAxmgAwIBAgIUQmYhdrAOzmtTBMJaa65q5PlEemIwDQYJKoZIhvcNAQEL\r\nBQAwgakxCzAJBgNVBAYTAk1ZMREwDwYDVQQIDAhTZWxhbmdvcjESMBAGA1UEBwwJ\r\nQ3liZXJqYXlhMRswGQYDVQQKDBJSdW5DbG91ZCBTZG4uIEJoZC4xFDASBgNVBAsM\r\nC0VuZ2luZWVyaW5nMSAwHgYDVQQDDBdSdW5DbG91ZCBXZWJTU0wgUm9vdCBDQTEe\r\nMBwGCSqGSIb3DQEJARYPZGV2QHJ1bmNsb3VkLmlvMB4XDTIxMDcwNDAyMDM0MFoX\r\nDTIzMTAwNDAyMDM0MFowcTETMBEGA1UECgwKTXkgQ29tcGFueTELMAkGA1UECwwC\r\nSVQxDTALBgNVBAcMBENpdHkxDjAMBgNVBAgMBVN0YXRlMQswCQYDVQQGDAJVUzEh\r\nMB8GA1UEAwwYUnVuQ2xvdWQgV2ViIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0B\r\nAQEFAAOCAQ8AMIIBCgKCAQEA1LAZqcYdFSWppXG6UvUSD0+44GZ5ThOkMi0cb6Fx\r\nI/m4tjbkd3AHvWzk1qeZhfMjpuhqrhjYQOG4DwACeACsBZCpxX5VFfi06ubHXTS0\r\nxHNLoi7LeR1S5td8yWUkhBZwUpTQ9wUWLC66TpNauRvn/R6PwwGQHIJk9cNDQHoy\r\nDQVd8MP44XV6+F5Dwp8eQiv8fDMbhBbO9IJguo2tA3jG9cxtpGL84mt+QKKGq8WG\r\nPalADWT0Q7DcOMVrK4FoytaiPJ5tjFqxmv6V/n3QCzRYyXQMM2dOOycveg8ImuXr\r\n3SlgMP4l8ZrsM/I8dn3/5uKvCvBWEhD1vFTsgjRLftooTwIDAQABo4GHMIGEMA4G\r\nA1UdDwEB/wQEAwIF4DAPBgNVHRMBAf8EBTADAgEAMB0GA1UdJQQWMBQGCCsGAQUF\r\nBwMBBggrBgEFBQcDAjAhBgNVHREEGjAYgglrdWNpbmcucmOCCyoua3VjaW5nLnJj\r\nMB8GA1UdIwQYMBaAFLLyby7gHA8E+5L84arEyafndfixMA0GCSqGSIb3DQEBCwUA\r\nA4IBAQBnMSKMz7DNm03OVrhPydxS6X6n2Ma6IucyWwrggPlQf0DjqZe8DpTZq+Zm\r\nRZmPvx0f4oL1JnWpRkG+BUTUlQtM0Db70ezrRqWGzll9tCQhkagrIGQGi51TeYf9\r\nY8lhTOwSIBP9c17jCuAxrAajZVcHW+jJTSc+ExFYBhIuiWQqH8PAFNfxMXRqZ6Lp\r\nDBz16iUqGRnweuus51jK1gZkr356k2RIQFErhXHQobuC48+bt0fWVl+VaDfaXpnT\r\nngG7TOtzGDnzwcOgNHHFMaRkb4PFMImx7ogHt5/7NtlWJh80YknO35q7Ba+weHpH\r\nzD+o3gRLl7EoQB/Wi48M7zCoRb5D\r\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIERDCCAyygAwIBAgIUZ8MfP1GxDkapNKf5PSBNF/KNifcwDQYJKoZIhvcNAQEL\nBQAwgakxCzAJBgNVBAYTAk1ZMREwDwYDVQQIDAhTZWxhbmdvcjESMBAGA1UEBwwJ\nQ3liZXJqYXlhMRswGQYDVQQKDBJSdW5DbG91ZCBTZG4uIEJoZC4xFDASBgNVBAsM\nC0VuZ2luZWVyaW5nMSAwHgYDVQQDDBdSdW5DbG91ZCBXZWJTU0wgUm9vdCBDQTEe\nMBwGCSqGSIb3DQEJARYPZGV2QHJ1bmNsb3VkLmlvMCAXDTIxMDQyMTA2MDgwN1oY\nDzIxMjEwMzI4MDYwODA3WjCBqTELMAkGA1UEBhMCTVkxETAPBgNVBAgMCFNlbGFu\nZ29yMRIwEAYDVQQHDAlDeWJlcmpheWExGzAZBgNVBAoMElJ1bkNsb3VkIFNkbi4g\nQmhkLjEUMBIGA1UECwwLRW5naW5lZXJpbmcxIDAeBgNVBAMMF1J1bkNsb3VkIFdl\nYlNTTCBSb290IENBMR4wHAYJKoZIhvcNAQkBFg9kZXZAcnVuY2xvdWQuaW8wggEi\nMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDs1I3diSCO/yrC8n2zkK8QVqup\nhWt8fYH9AcqfuvB0OV4/a3U6ojMvm9fRF3Vw0woI3lfFoklNDspe3SK9O01yBaGm\nCDU1FMEH6qg2X9LVT5ol9MYgHQRHzj/rWX3CVhzm3sRP7tUKRWVVE+8EZFl0I5bk\nCpzdRc619iSOneTb5Xb9db0A7EcvagvPOrqQzvRZ/RuLyHbNBzfZ90IXkobjTmbq\nihMYDwtFODkArI8k8iG7AXoE12E+j620TzV2rbi7FqZRX9elF8zF5jOijytG4fdr\nDrbXAlGArt3o3P8R99+GXBLhBwHW3PBKxHcU4kzZ4zmMM0+MpajbQwyAg0xzAgMB\nAAGjYDBeMB0GA1UdDgQWBBSy8m8u4BwPBPuS/OGqxMmn53X4sTAfBgNVHSMEGDAW\ngBSy8m8u4BwPBPuS/OGqxMmn53X4sTAMBgNVHRMEBTADAQH/MA4GA1UdDwEB/wQE\nAwIBhjANBgkqhkiG9w0BAQsFAAOCAQEAkDZi83HNJZZDmA3gqX/s2iZF6ooc608g\nBxTRIcPKgLn7aCIlosXOCtmj4rSeLJPARTs1oNUKTdME107MFINDEwjZFkxqb61X\n8kae9q64gQdn4a3k39f+Wjwc9pnklTvaHG0HJbW3zcQhtPU76dW2pstBEJc08jKD\nGqQkmrQSLMQ0RlL1jHnnKBZ6jQtbkS/6QGp3lmVhVvjhlKSnMXChqx4mEliICXi+\nfKozw+SfuwkTyyApJXzjirgy49s+8HE1vlz2ZPG+pg+p6yF/jzkWNjptalbtJjYY\nrPFsJch3y+PidsWl1tP1MCp7IVvs4+o0joIejRRYPlcVQxv9eUP52g==\n-----END CERTIFICATE-----\n",
	"validUntil": "2023-10-04 02:03:40",
	"renewalDate": null,
	"enableHttp": false,
	"enableHsts": true,
	"authorizationMethod": null,
	"staging": false,
	"api_id": null,
	"ssl_protocol_id": 2,
	"created_at": "2021-07-06 02:03:40"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Advanced - Update SSL Config

###### <span class="text-danger">Note : This API endpoint is available for Business plan user only. For non Business user, 403 HTTP response is returned.</span>

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>enableHttp</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean true or false</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>enableHsts</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean true or false</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>ssl_protocol_id</span>
                <span class="text-danger">OPTIONAL</span>
            </td>
            <td>Integer</td>
            <td>SSL Protocol. You can get this value from Static Data API. It will current value if not supplied.</td>
        </tr>
        <tr>
            <td>
                <span>privateKey</span>
                <span>required if provider is "custom"</span>
            </td>
            <td>SSL Private Key</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>certificate</span>
                <span>required if provider is "custom"</span>
            </td>
            <td>SSL Certificate</td>
            <td></td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl/{sslId}`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl/{sslId}
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/domains/11/ssl/10 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "enableHttp": true,
  "enableHsts": false,
  "ssl_protocol_id": 2,
  "privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDem6F7HLVxStq/\n/t6aThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0\nzkVT2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCg\nuVkSehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5am\nLU3iSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWro\nCEIQ61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvo\nrn+YPEvDAgMBAAECggEAXOcIqo+dra12w8ADeNRxx8LdW+IMaLOLz6euxbKLUzd9\nlQC0WIKf86jR66B7uljr6HVPsNOXb5rKt/wJ9OdTUW33nn3YKe/41j1VfnbD1pme\nHWFwGP+LpINt7EvzsFEYNDiwV7p+neUqVyljYhGtlBJI70+5dEg4ihBVuWolcQai\nH06XxdT72O41RKr/uSzKcuxZBQhdD9Y505UVMsHOjFu0HMUMZ/wk7msZg4nyYmfg\nW0h/LbzmzblbEzF+wd4PYTE5FC4SRiS1/E8aDe78Rg7ww7pxgW9OvGlMWv4lrfco\nBO430h+VBodQnHHaTj4lV59pYhQGvlHwkSDEHpuWAQKBgQD3Jh6sIY97kOCJ9oET\nlhsBZ6JICiqmsieWRuSmMoiErBezVZ9QS5C5GOL4GkPsc7Bw0s6RB+9TZsEPS3DH\nglpzr5a89d3BRlkSNFSSHeq/W8/s8DYvMoQeapYTr4ijH97y18IjChozx5fcmzQS\nY8J0hYOruabZgyjBvSABNxokwwKBgQDmlIR3Mj7YQsSyyvgOp0r0e1u2zNnnm5n6\nC/kZR1SuhVjQ2tdwPafqjdSoFimUMPrSmJBT6U7LqfEtRaByR9xEHR0d3qb8Ofni\nSngD4NWmPSX6GnWwJVsPHDSqn6JW1eEdAwMisB7F4FhRmvdffmIaVI89HZSuv5gl\n/uEQRGbNAQKBgH+ODQaJy6PaggiyUKvrLMTs17SWiqy+BfBpZljge9T9fL3x0ud+\nGJNvZLTn2WaPzuBr7HCtx7cjsUBTj0Fo5YYPeZzMyEaYKCBdIcjH6AAbQpTm5RA2\n4jlQiWRflAWczVRIRsoOzLcsrBQPhjB3jETXI73dc1+PcdmL4pi996BBAoGAEuob\n86srfJH9kK0VrB4NCAEWhOhI97bL6rcQuAIh8C8AGiHZiluEark3uJIY1w8thBj/\nveJllE9ceVo8zyMV7oB04v5gtFANL4LsVWUcIYbilqGVBd4KmjK2H0j5CCaDUN3u\nY+oOnCzLEeakZDD52y8UkO7cQ3l0AanmzG7QAAECgYBVTTgYqAoUG6wVuQw5Jrnz\n9/ItvU4zPKDadnoIRgMZq+wXo8f8dMSzNjlylv+vQF99E9NRMMIVbyeH4/q5ANGl\nkcFwSyAhE7z+uSV0Ho6OqZVqis8e9rcin9+opC5BePQAjgAyAYciXpE95YXQcYMM\nlXCUsdHQomIAwFN2TNWohQ==\n-----END PRIVATE KEY-----",
    "certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqACCQC6DH7qQzl7LTANBgkqhkiG9w0BAQsFADCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wHhcNMTgwMzAyMDMyNjI3WhcNMTkwMzAyMDMyNjI3WjCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDem6F7HLVxStq//t6a\nThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0zkVT\n2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCguVkS\nehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5amLU3i\nSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWroCEIQ\n61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvorn+Y\nPEvDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA+HxyKESRAzUmDt1aE0v6u95QBB\niGGhVszAviF6ZKaf6bov0Ozq+67Pq+3Tksu685Giux+ZSWIlDi3zL4NWz+rcvIhO\nKoVAPPYawIAhOszSg3IGD0eVP1I7al5sME7GZPM3kPwxTNCv8z91mPaSyI5/k88c\nm62Fijn4k92kWDOMF7ANFST6JhujB8OitHLFjB1Rapi0H0TpgAtiHCNYcgmFL66e\nlOkmbGzzjdQGIGfY7H3owmkuI5FMycV5TYDkLChasYSlW0sQSJD1j1CiM+qSAE7Y\npWiuEVjrSYgJlcQfBnpQjkjAp2F4bp0TDQT7e+MNCOfA+lUdSWKyZVTLHF0=\n-----END CERTIFICATE-----"
}'
```

###### Response

```json
{
	"id": 10,
	"method": "custom",
	"privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDem6F7HLVxStq/\n/t6aThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0\nzkVT2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCg\nuVkSehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5am\nLU3iSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWro\nCEIQ61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvo\nrn+YPEvDAgMBAAECggEAXOcIqo+dra12w8ADeNRxx8LdW+IMaLOLz6euxbKLUzd9\nlQC0WIKf86jR66B7uljr6HVPsNOXb5rKt/wJ9OdTUW33nn3YKe/41j1VfnbD1pme\nHWFwGP+LpINt7EvzsFEYNDiwV7p+neUqVyljYhGtlBJI70+5dEg4ihBVuWolcQai\nH06XxdT72O41RKr/uSzKcuxZBQhdD9Y505UVMsHOjFu0HMUMZ/wk7msZg4nyYmfg\nW0h/LbzmzblbEzF+wd4PYTE5FC4SRiS1/E8aDe78Rg7ww7pxgW9OvGlMWv4lrfco\nBO430h+VBodQnHHaTj4lV59pYhQGvlHwkSDEHpuWAQKBgQD3Jh6sIY97kOCJ9oET\nlhsBZ6JICiqmsieWRuSmMoiErBezVZ9QS5C5GOL4GkPsc7Bw0s6RB+9TZsEPS3DH\nglpzr5a89d3BRlkSNFSSHeq/W8/s8DYvMoQeapYTr4ijH97y18IjChozx5fcmzQS\nY8J0hYOruabZgyjBvSABNxokwwKBgQDmlIR3Mj7YQsSyyvgOp0r0e1u2zNnnm5n6\nC/kZR1SuhVjQ2tdwPafqjdSoFimUMPrSmJBT6U7LqfEtRaByR9xEHR0d3qb8Ofni\nSngD4NWmPSX6GnWwJVsPHDSqn6JW1eEdAwMisB7F4FhRmvdffmIaVI89HZSuv5gl\n/uEQRGbNAQKBgH+ODQaJy6PaggiyUKvrLMTs17SWiqy+BfBpZljge9T9fL3x0ud+\nGJNvZLTn2WaPzuBr7HCtx7cjsUBTj0Fo5YYPeZzMyEaYKCBdIcjH6AAbQpTm5RA2\n4jlQiWRflAWczVRIRsoOzLcsrBQPhjB3jETXI73dc1+PcdmL4pi996BBAoGAEuob\n86srfJH9kK0VrB4NCAEWhOhI97bL6rcQuAIh8C8AGiHZiluEark3uJIY1w8thBj/\nveJllE9ceVo8zyMV7oB04v5gtFANL4LsVWUcIYbilqGVBd4KmjK2H0j5CCaDUN3u\nY+oOnCzLEeakZDD52y8UkO7cQ3l0AanmzG7QAAECgYBVTTgYqAoUG6wVuQw5Jrnz\n9/ItvU4zPKDadnoIRgMZq+wXo8f8dMSzNjlylv+vQF99E9NRMMIVbyeH4/q5ANGl\nkcFwSyAhE7z+uSV0Ho6OqZVqis8e9rcin9+opC5BePQAjgAyAYciXpE95YXQcYMM\nlXCUsdHQomIAwFN2TNWohQ==\n-----END PRIVATE KEY-----",
	"certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqACCQC6DH7qQzl7LTANBgkqhkiG9w0BAQsFADCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wHhcNMTgwMzAyMDMyNjI3WhcNMTkwMzAyMDMyNjI3WjCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDem6F7HLVxStq//t6a\nThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0zkVT\n2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCguVkS\nehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5amLU3i\nSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWroCEIQ\n61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvorn+Y\nPEvDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA+HxyKESRAzUmDt1aE0v6u95QBB\niGGhVszAviF6ZKaf6bov0Ozq+67Pq+3Tksu685Giux+ZSWIlDi3zL4NWz+rcvIhO\nKoVAPPYawIAhOszSg3IGD0eVP1I7al5sME7GZPM3kPwxTNCv8z91mPaSyI5/k88c\nm62Fijn4k92kWDOMF7ANFST6JhujB8OitHLFjB1Rapi0H0TpgAtiHCNYcgmFL66e\nlOkmbGzzjdQGIGfY7H3owmkuI5FMycV5TYDkLChasYSlW0sQSJD1j1CiM+qSAE7Y\npWiuEVjrSYgJlcQfBnpQjkjAp2F4bp0TDQT7e+MNCOfA+lUdSWKyZVTLHF0=\n-----END CERTIFICATE-----",
	"validUntil": "2019-03-02 03:26:27",
	"renewalDate": null,
	"enableHttp": true,
	"enableHsts": false,
	"authorizationMethod": null,
	"staging": false,
	"api_id": null,
	"ssl_protocol_id": 2,
	"created_at": "2019-06-30 11:37:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Advanced - Redeploy SSL (Let's Encrypt only)

###### <span class="text-danger">Note : This API endpoint is available for Business plan user only. For non Business user, 403 HTTP response is returned.</span>

</div>
<div class="col col-xl-6 dark">

`PUT /servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl/{sslId}`

###### HTTP Request

```
PUT https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl/{sslId}
```

###### Request

```bash
curl --request PUT \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/domains/11/ssl/10 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 10,
	"method": "letsencrypt",
	"privateKey": null,
	"certificate": null,
	"validUntil": null,
	"renewalDate": null,
	"enableHttp": true,
	"enableHsts": false,
	"authorizationMethod": "dns-01",
	"staging": true,
	"api_id": 2,
	"ssl_protocol_id": 2,
	"created_at": "2019-06-30 13:43:38"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### SSL Advanced - Delete SSL

###### <span class="text-danger">Note : This API endpoint is available for Business plan user only. For non Business user, 403 HTTP response is returned.</span>

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl/{sslId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/domains/{domainId}/ssl/{sslId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/domains/11/ssl/10 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 10,
	"method": "custom",
	"privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDem6F7HLVxStq/\n/t6aThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0\nzkVT2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCg\nuVkSehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5am\nLU3iSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWro\nCEIQ61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvo\nrn+YPEvDAgMBAAECggEAXOcIqo+dra12w8ADeNRxx8LdW+IMaLOLz6euxbKLUzd9\nlQC0WIKf86jR66B7uljr6HVPsNOXb5rKt/wJ9OdTUW33nn3YKe/41j1VfnbD1pme\nHWFwGP+LpINt7EvzsFEYNDiwV7p+neUqVyljYhGtlBJI70+5dEg4ihBVuWolcQai\nH06XxdT72O41RKr/uSzKcuxZBQhdD9Y505UVMsHOjFu0HMUMZ/wk7msZg4nyYmfg\nW0h/LbzmzblbEzF+wd4PYTE5FC4SRiS1/E8aDe78Rg7ww7pxgW9OvGlMWv4lrfco\nBO430h+VBodQnHHaTj4lV59pYhQGvlHwkSDEHpuWAQKBgQD3Jh6sIY97kOCJ9oET\nlhsBZ6JICiqmsieWRuSmMoiErBezVZ9QS5C5GOL4GkPsc7Bw0s6RB+9TZsEPS3DH\nglpzr5a89d3BRlkSNFSSHeq/W8/s8DYvMoQeapYTr4ijH97y18IjChozx5fcmzQS\nY8J0hYOruabZgyjBvSABNxokwwKBgQDmlIR3Mj7YQsSyyvgOp0r0e1u2zNnnm5n6\nC/kZR1SuhVjQ2tdwPafqjdSoFimUMPrSmJBT6U7LqfEtRaByR9xEHR0d3qb8Ofni\nSngD4NWmPSX6GnWwJVsPHDSqn6JW1eEdAwMisB7F4FhRmvdffmIaVI89HZSuv5gl\n/uEQRGbNAQKBgH+ODQaJy6PaggiyUKvrLMTs17SWiqy+BfBpZljge9T9fL3x0ud+\nGJNvZLTn2WaPzuBr7HCtx7cjsUBTj0Fo5YYPeZzMyEaYKCBdIcjH6AAbQpTm5RA2\n4jlQiWRflAWczVRIRsoOzLcsrBQPhjB3jETXI73dc1+PcdmL4pi996BBAoGAEuob\n86srfJH9kK0VrB4NCAEWhOhI97bL6rcQuAIh8C8AGiHZiluEark3uJIY1w8thBj/\nveJllE9ceVo8zyMV7oB04v5gtFANL4LsVWUcIYbilqGVBd4KmjK2H0j5CCaDUN3u\nY+oOnCzLEeakZDD52y8UkO7cQ3l0AanmzG7QAAECgYBVTTgYqAoUG6wVuQw5Jrnz\n9/ItvU4zPKDadnoIRgMZq+wXo8f8dMSzNjlylv+vQF99E9NRMMIVbyeH4/q5ANGl\nkcFwSyAhE7z+uSV0Ho6OqZVqis8e9rcin9+opC5BePQAjgAyAYciXpE95YXQcYMM\nlXCUsdHQomIAwFN2TNWohQ==\n-----END PRIVATE KEY-----",
	"certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqACCQC6DH7qQzl7LTANBgkqhkiG9w0BAQsFADCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wHhcNMTgwMzAyMDMyNjI3WhcNMTkwMzAyMDMyNjI3WjCBnTELMAkGA1UEBhMC\nTVkxDjAMBgNVBAgMBUpvaG9yMQ8wDQYDVQQHDAZTa3VkYWkxGzAZBgNVBAoMElJ1\nbkNsb3VkIFNkbi4gQmhkLjEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGDAWBgNVBAMM\nD3d3dy5leGFtcGxlLmNvbTEgMB4GCSqGSIb3DQEJARYRaGVsbG9AcnVuY2xvdWQu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDem6F7HLVxStq//t6a\nThEEntPgIjjgj1wL03hVTIHbDhzZ/hWh7KEo5nQCdL+HMJIGnpM/7htEVos0zkVT\n2QchP6p0p9PR1rY3+SVoxEcltJ9xug8EZ7Zut8F5VHjbNsTi7W0S3irj6MCguVkS\nehE4PeV9P+zeXf+0Aa7KwDiQHI1krvqY9DQda4IGyPeZI3cFbzFBYHhUK5amLU3i\nSpwNdBPWsgxQNNtsFnfOvmhuxxZ8B55TKATAVZ6UDI4DR1ysyCsJv8bajWroCEIQ\n61dyJb+H5eyTrd86qN46mYgGLNdoClOhQ4QISoz8mhWzFf/oIUbiHzBpslvorn+Y\nPEvDAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA+HxyKESRAzUmDt1aE0v6u95QBB\niGGhVszAviF6ZKaf6bov0Ozq+67Pq+3Tksu685Giux+ZSWIlDi3zL4NWz+rcvIhO\nKoVAPPYawIAhOszSg3IGD0eVP1I7al5sME7GZPM3kPwxTNCv8z91mPaSyI5/k88c\nm62Fijn4k92kWDOMF7ANFST6JhujB8OitHLFjB1Rapi0H0TpgAtiHCNYcgmFL66e\nlOkmbGzzjdQGIGfY7H3owmkuI5FMycV5TYDkLChasYSlW0sQSJD1j1CiM+qSAE7Y\npWiuEVjrSYgJlcQfBnpQjkjAp2F4bp0TDQT7e+MNCOfA+lUdSWKyZVTLHF0=\n-----END CERTIFICATE-----",
	"validUntil": "2019-03-02 03:26:27",
	"renewalDate": null,
	"enableHttp": true,
	"enableHsts": false,
	"authorizationMethod": null,
	"staging": false,
	"api_id": null,
	"ssl_protocol_id": 2,
	"created_at": "2019-06-30 11:37:29"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Get Web Application settings

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}/settings`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/settings
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/settings \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"disableFunctions": "getmyuid,passthru,leak,listen,diskfreespace,tmpfile,link,ignore_user_abort,shell_exec,dl,set_time_limit,exec,system,highlight_file,source,show_source,fpassthru,virtual,posix_ctermid,posix_getcwd,posix_getegid,posix_geteuid,posix_getgid,posix_getgrgid,posix_getgrnam,posix_getgroups,posix_getlogin,posix_getpgid,posix_getpgrp,posix_getpid,posix,_getppid,posix_getpwuid,posix_getrlimit,posix_getsid,posix_getuid,posix_isatty,posix_kill,posix_mkfifo,posix_setegid,posix_seteuid,posix_setgid,posix_setpgid,posix_setsid,posix_setuid,posix_times,posix_ttyname,posix_uname,proc_open,proc_close,proc_nice,proc_terminate,escapeshellcmd,ini_alter,popen,pcntl_exec,socket_accept,socket_bind,socket_clear_error,socket_close,socket_connect,symlink,posix_geteuid,ini_alter,socket_listen,socket_create_listen,socket_read,socket_create_pair,stream_socket_server",
	"timezone": "UTC",
	"maxExecutionTime": 30,
	"maxInputTime": 60,
	"maxInputVars": 1000,
	"memoryLimit": 256,
	"postMaxSize": 256,
	"uploadMaxFilesize": 256,
	"allowUrlFopen": true,
	"sessionGcMaxlifetime": 1440,
	"processManager": "ondemand",
	"processManagerStartServers": null,
	"processManagerMinSpareServers": null,
	"processManagerMaxSpareServers": null,
	"processManagerMaxChildren": 50,
	"processManagerMaxRequests": 500,
	"openBasedir": "/home/myuser/webapps/testing:/var/lib/php/session:/tmp",
	"clickjackingProtection": true,
	"xssProtection": true,
	"mimeSniffingProtection": true
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Change PHP version

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>phpVersion</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>Refer to Server section on how to get list of PHP version supported</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/webapps/{webAppId}/settings/php`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/settings/php
```

###### Request

```bash
curl --request PATCH \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/settings/php \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "phpVersion": "php72rc"
}'
```

###### Response

```json
{
	"id": 109,
	"server_user_id": 142,
	"name": "testing",
	"rootPath": "/home/myuser/webapps/testing",
	"publicPath": "/home/myuser/webapps/testing",
	"phpVersion": "php72rc",
	"stack": "hybrid",
	"stackMode": "production",
	"type": "custom",
	"defaultApp": false,
	"alias": null,
	"pullKey1": "jwMZwtXP3ItQRKKoMSZboAXr1561748870",
	"pullKey2": "zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu",
	"created_at": "2019-06-28 19:07:50"
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Update PHP-FPM, NGiNX settings

###### Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>publicPath</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Append a public path to a Web Application. If not defined, Web Application root path will be used as publicPath</td>
        </tr>
        <tr>
            <td>
                <span>stack</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"hybrid", "nativenginx", or "customnginx"</td>
        </tr><tr>
            <td>
                <span>stackMode</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"production" or "development"</td>
        </tr>
        <tr>
            <td>
                <span>clickjackingProtection</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>xssProtection</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>mimeSniffingProtection</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManager</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>"dynamic", "ondemand", or "static"</td>
        </tr>
        <tr>
            <td>
                <span>processManagerStartServers</span>
                <span>required if processManager is "dynamic"</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManagerMinSpareServers</span>
                <span>required if processManager is "dynamic"</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManagerMaxSpareServers</span>
                <span>required if processManager is "dynamic"</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManagerMaxChildren</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>processManagerMaxRequests</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <span>openBasedir</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>If you left this empty, open_basedir will be empty. To add a default value, use "/home/{systemUser.username}/webapps/{webApp.name}:/var/lib/php/session:/tmp"</td>
        </tr>
        <tr>
            <td>
                <span>timezone</span>
                <span class="text-danger">required</span>
            </td>
            <td>String</td>
            <td>You can get this from Static Data for Timezone</td>
        </tr>
        <tr>
            <td>
                <span>disableFunctions</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>If you left this empty, no php functions will be disabled. If you wanted to supply the default value, see the request example</td>
        </tr>
        <tr>
            <td>
                <span>maxExecutionTime</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>maxInputTime</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>maxInputVars</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>memoryLimit</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Supplied value is in MB.</td>
        </tr>
        <tr>
            <td>
                <span>postMaxSize</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Supplied value is in MB. Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>uploadMaxFilesize</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Supplied value is in MB. Use 0 for unlimited</td>
        </tr>
        <tr>
            <td>
                <span>sessionGcMaxlifetime</span>
                <span class="text-danger">required</span>
            </td>
            <td>Integer</td>
            <td>Supplied value is in seconds</td>
        </tr>
        <tr>
            <td>
                <span>allowUrlFopen</span>
                <span class="text-danger">required</span>
            </td>
            <td>Boolean</td>
            <td></td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`PATCH /servers/{serverId}/webapps/{webAppId}/settings/fpmnginx`

###### HTTP Request

```
PATCH https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/settings/fpmnginx
```

###### Request

```bash
curl --request PATCH \
  --url 'https://{{ API_BASE_PATH }}/servers/7/webapps/109/settings/fpmnginx' \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
  "publicPath": null,
  "stack": "nativenginx",
  "stackMode": "development",
  "clickjackingProtection": true,
  "xssProtection": true,
  "mimeSniffingProtection": true,
  "processManager": "ondemand",
  "processManagerMaxChildren": 50,
  "processManagerMaxRequests": 500,
  "openBasedir": "/home/myuser/webapps/testing:/var/lib/php/session:/tmp",
  "timezone": "UTC",
  "disableFunctions": "getmyuid,passthru,leak,listen,diskfreespace,tmpfile,link,ignore_user_abort,shell_exec,dl,set_time_limit,exec,system,highlight_file,source,show_source,fpassthru,virtual,posix_ctermid,posix_getcwd,posix_getegid,posix_geteuid,posix_getgid,posix_getgrgid,posix_getgrnam,posix_getgroups,posix_getlogin,posix_getpgid,posix_getpgrp,posix_getpid,posix,_getppid,posix_getpwuid,posix_getrlimit,posix_getsid,posix_getuid,posix_isatty,posix_kill,posix_mkfifo,posix_setegid,posix_seteuid,posix_setgid,posix_setpgid,posix_setsid,posix_setuid,posix_times,posix_ttyname,posix_uname,proc_open,proc_close,proc_nice,proc_terminate,escapeshellcmd,ini_alter,popen,pcntl_exec,socket_accept,socket_bind,socket_clear_error,socket_close,socket_connect,symlink,posix_geteuid,ini_alter,socket_listen,socket_create_listen,socket_read,socket_create_pair,stream_socket_server",
  "maxExecutionTime": 30,
  "maxInputTime": 60,
  "maxInputVars": 1000,
  "memoryLimit": 256,
  "postMaxSize": 256,
  "uploadMaxFilesize": 256,
  "sessionGcMaxlifetime": 1440,
  "allowUrlFopen": true
}'
```

###### Response

```json
{
	"id": 109,
	"server_user_id": 142,
	"name": "testing",
	"rootPath": "/home/myuser/webapps/testing",
	"publicPath": "/home/myuser/webapps/testing",
	"phpVersion": "php72rc",
	"stack": "nativenginx",
	"stackMode": "development",
	"type": "custom",
	"defaultApp": false,
	"alias": null,
	"pullKey1": "jwMZwtXP3ItQRKKoMSZboAXr1561748870",
	"pullKey2": "zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu",
	"created_at": "2019-06-28 19:07:50",
	"setting": {
		"disableFunctions": "getmyuid,passthru,leak,listen,diskfreespace,tmpfile,link,ignore_user_abort,shell_exec,dl,set_time_limit,exec,system,highlight_file,source,show_source,fpassthru,virtual,posix_ctermid,posix_getcwd,posix_getegid,posix_geteuid,posix_getgid,posix_getgrgid,posix_getgrnam,posix_getgroups,posix_getlogin,posix_getpgid,posix_getpgrp,posix_getpid,posix,_getppid,posix_getpwuid,posix_getrlimit,posix_getsid,posix_getuid,posix_isatty,posix_kill,posix_mkfifo,posix_setegid,posix_seteuid,posix_setgid,posix_setpgid,posix_setsid,posix_setuid,posix_times,posix_ttyname,posix_uname,proc_open,proc_close,proc_nice,proc_terminate,escapeshellcmd,ini_alter,popen,pcntl_exec,socket_accept,socket_bind,socket_clear_error,socket_close,socket_connect,symlink,posix_geteuid,ini_alter,socket_listen,socket_create_listen,socket_read,socket_create_pair,stream_socket_server",
		"timezone": "UTC",
		"maxExecutionTime": 30,
		"maxInputTime": 60,
		"maxInputVars": 1000,
		"memoryLimit": 256,
		"postMaxSize": 256,
		"uploadMaxFilesize": 256,
		"allowUrlFopen": true,
		"sessionGcMaxlifetime": 1440,
		"processManager": "ondemand",
		"processManagerStartServers": null,
		"processManagerMinSpareServers": null,
		"processManagerMaxSpareServers": null,
		"processManagerMaxChildren": 50,
		"processManagerMaxRequests": 500,
		"openBasedir": "/home/myuser/webapps/testing:/var/lib/php/session:/tmp",
		"clickjackingProtection": true,
		"xssProtection": true,
		"mimeSniffingProtection": true
	}
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Web Application action log

###### Query String Parameters

<div class="table-responsive">
<table class="table">
    <tbody>
        <tr>
            <td>
                <span>search</span>
                <span>optional</span>
            </td>
            <td>String</td>
            <td>Filter log</td>
        </tr>
    </tbody>
</table>
</div>

</div>
<div class="col col-xl-6 dark">

`GET /servers/{serverId}/webapps/{webAppId}/log`

###### HTTP Request

```
GET https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}/log
```

###### Request

```bash
curl --request GET \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109/log \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
  "data": [
    {
      "kind": "Warning",
      "content": "Web Application settings was modified",
      "created_at": "2019-06-30 14:56:18"
    },
    {
      "kind": "Warning",
      "content": "Web Application settings was modified",
      "created_at": "2019-06-30 14:55:09"
    },
    {
      "kind": "Warning",
      "content": "Web Application settings was modified",
      "created_at": "2019-06-30 14:52:52"
    },
    ...
    ...
    ...
  ],
  "meta": {
    "pagination": {
      "total": 73,
      "count": 15,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 5,
      "links": {
        "next": "https:\/\/runcloud-gateway.test\/api\/v2\/servers\/7\/webapps\/109\/log?page=2"
      }
    }
  }
}
```

</div>
</div>

<div class="row">
<div class="col col-xl-6">

#### Delete web application

</div>
<div class="col col-xl-6 dark">

`DELETE /servers/{serverId}/webapps/{webAppId}`

###### HTTP Request

```
DELETE https://{{ API_BASE_PATH }}/servers/{serverId}/webapps/{webAppId}
```

###### Request

```bash
curl --request DELETE \
  --url https://{{ API_BASE_PATH }}/servers/7/webapps/109 \
  -u YOUR_API_KEY:YOUR_API_SECRET \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

###### Response

```json
{
	"id": 109,
	"server_user_id": 142,
	"name": "testing",
	"rootPath": "/home/myuser/webapps/testing",
	"publicPath": "/home/myuser/webapps/testing",
	"phpVersion": "php72rc",
	"stack": "nativenginx",
	"stackMode": "production",
	"type": "custom",
	"defaultApp": false,
	"alias": null,
	"pullKey1": "jwMZwtXP3ItQRKKoMSZboAXr1561748870",
	"pullKey2": "zU4gYF96NZGjNqGSjUhasn0YZmlK2Ctu",
	"created_at": "2019-06-28 19:07:50"
}
```

</div>
</div>
